saving ner to database gives error with entries like "M."
Need to make sure there are no dots in entities


relations:
    - 
    - CapableOf : 2.5 : 1
    - AtLocation :  2.5 : 1 
    - CreatedBy : 1.5 : 1
    - DefinedAs : 1.1 : 3
    - DerivedFrom : 1.5 : 1 
    - HasA : 2.0 : 2
    - HasContext : 1.0 : 1
    - HasFirstSubevent : 2 : 1 
    - HasPrerequiste : 2.5 : 1
    - HasProperty : 2 : 0.5
    - HasSubevent : 2 : 0.5
    - InstanceOf : 1 : 2
    - IsA : 1.5 : 2
    - MadeOf :  1.5 : 2
    - MemberOf : 1.5 : 2
    - Mot : 2.0 : 1
    - /r/MotivatedByGoal : 2.0 : 1
    - /r/PartOf : 2.0 : 2
    - /r/ReceivesAction : 2.0 : 1
    - /r/RelatedTo : 2.0 : 1
    - /r/SimilarTo : 2.5 : 2
    - /r/Synonym : 1 : 3
    - /r/TranslationOf : 1 : 8
    - /r/UsedFor : 2.5 : 1
    - /r/wordnet/adjectivePertainsTo : 1.0 : 1
    - /r/wordnet/adverbPertainsTo : 1.0 : 1

    

relations ban:  
    - 
    - Antonymns 
    - Causes 
    - CausesDesire
    - CompoundDerivedFrom 
    - DesireOf 
    - Desires
    - Entails
    - EtymologicallyDerivedFrom
    - /r/NotCapableOf
    - /r/NotDesires
    - /r/NotHasA
    - /r/NotHasProperty
    - /r/NotIsA
    - 



relations Uncertain:
    -   
    - Attribute
    - dpedia/genre 
    