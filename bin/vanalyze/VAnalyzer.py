from pymongo import MongoClient
import resources as rs
import mapper as mp
import bin.insight.resources as rsInsight


class BaseAnalyzer(object):
    def __init__(self, HOST, LANG, MAXSIZE=10):
        self._client = MongoClient(HOST)
        self._lang = LANG
        self._maxsize = MAXSIZE

    def _get_verbatims(self, ids):
        return [v for v in self._client.indexes.verbatims.find({"_id": {"$in": ids}})]

    def get_similarities(self, ids):
        output = {
            "words": self._get_similiar_single_grams(ids)
        }
        return output

    def _get_similiar_single_grams(self, ids):
        mapperList = {}
        mapper = mp.SingleGramMapper([rsInsight.stop_words[self._lang]])
        for colName in rs.LIST_SINGLE_GRAMS:
            col = self._client.indexes[colName]
            mapper.reset()
            # fetching items
            for item in col.find({"_id": {"$in": ids}}):
                for i, word in enumerate(item[rs.COLLECTION_CONTENT_MAPPING[colName]['g']]):
                    mapper.map(word, item['_id'])
            # finally append mapper to list of mappers
            mapperList[rs.COLLECTION_CONTENT_MAPPING[colName]['name']] = mapper.get_map()

        # filter mapper lists to only contain entries relevant for all verbatims
        orderMap = {}
        for mapperName in mapperList:
            map = mapperList[mapperName]
            for e in map:
                length = len(map[e])
                if length > 1:
                    if length not in orderMap:
                        orderMap[length] = {}
                    if mapperName not in orderMap[length]:
                        orderMap[length][mapperName] = {}

                    if len(orderMap[length][mapperName]) < self._maxsize:  # restrict the size of the final object
                        # make sure no repeats
                        ifsave = True
                        for i in orderMap:
                            for k in orderMap[i]:
                                if e in orderMap[i][k]:
                                    ifsave = False
                        if ifsave:
                            orderMap[length][mapperName][e] = map[e]

        return orderMap
