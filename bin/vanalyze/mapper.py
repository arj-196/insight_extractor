class Mapper(object):
    pass


class SingleGramMapper(Mapper):
    _map = {}

    def __init__(self, filterList):
        self.filterList = filterList

    def reset(self):
        self._map = {}

    def map(self, word, value):
        iftreat = True

        # if word in any filter list, ignore word
        for fl in self.filterList:
            if word.encode('utf8') in fl:
                iftreat = False

        if iftreat:
            if word not in self._map:
                self._map[word] = [value]
            else:
                if value not in self._map[word]:
                    self._map[word].append(value)

    def get_map(self):
        return self._map
