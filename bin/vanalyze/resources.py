# map of collections to single grams and their weights
COLLECTION_CONTENT_MAPPING = {
    "verbatimadjectives": {"g": "words", "w": "weights", "name": "adjectives"},
    "verbatimadverbs": {"g": "words", "w": "weights", "name": "verbs"},
    "verbatimconcepts": {"g": "concepts", "w": "weights", "name": "concepts"},
    "verbatimkeywords": {"g": "keywords", "w": "weight", "name": "keywords"},
    "verbatimnouns": {"g": "words", "w": "weights", "name": "nouns"},
    "verbatimtags": {"g": "tags", "w": None, "name": "tags"},
    "verbatimterms": {"g": "terms", "w": None, "name": "terms"},
    "verbatimverbs": {"g": "words", "w": "weights", "name": "verbs"},
}

# list of collections that contain single gram format data
LIST_SINGLE_GRAMS = [
    'verbatimadjectives',
    'verbatimadverbs',
    'verbatimkeywords',
    'verbatimnouns',
    'verbatimterms',
    'verbatimverbs',
]
