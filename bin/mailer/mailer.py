import smtplib
from os.path import basename
import os
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
import resources as rs


class Mailer(object):
    def __init__(self):
        self.smtpserver = smtplib.SMTP(rs.SMTPHOST)
        self.smtpserver.ehlo()
        self.smtpserver.starttls()
        self.smtpserver.login(rs.USER, rs.PWD)

    def __del__(self):
        self.smtpserver.close()

    def _create_message(self, _from, to, subject):
        msg = MIMEMultipart('alternative')
        msg['Subject'] = subject
        msg['From'] = _from
        msg['To'] = to
        return msg

    def _create_body(self, subject, body):
        text = body
        html = """\
        <html>
          <head></head>
          <body>
            <h1>""" + subject + """ </h1>
            <p> """ + body + """ </p>
          </body>
        </html>
        """
        return text, html

    def _attach_file(self, msg, _file):
        if os.path.exists(_file):
            with open(_file, "rb") as fil:
                msg.attach(MIMEApplication(
                    fil.read(),
                    Content_Disposition='attachment; filename="%s"' % basename(_file),
                    Name=basename(_file)
                ))
        return msg

    def send(self, _from, to, subject, body, _file=None):
        # create message
        msg = self._create_message(_from, to, subject)
        # creating and attaching body
        text, html = self._create_body(subject, body)
        part1 = MIMEText(text, 'plain')
        part2 = MIMEText(html, 'html')
        msg.attach(part1)
        msg.attach(part2)

        if _file:
            msg = self._attach_file(msg, _file)

        self.smtpserver.sendmail(rs.USER, to, msg.as_string())


class ErrorAlerter(Mailer):
    def alert_error(self, subject, body, _file=None):
        for ADDR in rs.REPORTTOADDRS:
            self.send(
                rs.INSIGHTADDR,
                ADDR,
                subject,
                body,
                _file
            )
