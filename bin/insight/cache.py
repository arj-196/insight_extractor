class CacheMap(object):
    cache = {}
    LIMITSIZE = 300000

    def has_value(self, key):
        return key in self.cache

    def set(self, key, value):
        if len(self.cache) < self.LIMITSIZE:
            self.cache[key] = value

    def get(self, key):
        return self.cache[key]
