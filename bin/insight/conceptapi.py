from urllib3 import HTTPConnectionPool
import json
# from debugger.debugger import Debugger
import sys, traceback
import resources as rs
from datetime import datetime
from conceptnet5.query import lookup
from pymongo import MongoClient
startTime = datetime.now()


class ConceptNetAPI(object):
    def __init__(self):
        self._client = MongoClient(rs.HOST)

    @staticmethod
    def _form_uri(word, lang):
        url = '/c/' + lang + '/' + word
        return url

    def get_concepts(self, word, lang):
        concepts = {}
        uri = self._form_uri(word, lang)
        # checking in cache
        cache = self.get_from_cache(uri)
        if cache is None:
            for edge in lookup(uri):
                if edge['rel'] in rs.CONCEPTNET_RELATION_MAPPING:
                    # self._log_relation(word, edge)  # log info
                    if edge['weight'] >= rs.CONCEPTNET_RELATION_MAPPING[edge['rel']]['threshold']:
                        key_concept = edge['end'] + "," + edge['rel']
                        if key_concept not in concepts:
                            concepts[key_concept] = {
                                'end': edge['end'], 'rel': edge['rel'],
                                'weight': edge['weight']
                            }
                        else:
                            if edge['weight'] > concepts[key_concept]['weight']:
                                # replace relationship
                                concepts[key_concept] = {
                                    'end': edge['end'], 'rel': edge['rel'],
                                    'weight': edge['weight']
                                }

                else:
                    if edge['rel'] not in rs.CONCEPTNET_RELATION_BAN_MAPPING:
                        # print "NEW RELATION FOUND!", edge['rel']
                        pass

            # store in cache
            self.store_in_cache(uri, concepts)
            return concepts
        else:
            return cache

    def get_from_cache(self, uri):
        o = self._client.cache.concepts.find_one({"_id": uri})
        if o and 'concepts' in o:
            return o['concepts']
        else:
            return None

    def store_in_cache(self, uri, concepts):
        self._client.cache.concepts.insert({"_id": uri, "concepts": concepts})


class ConceptNetWebAPI(object):
    pool = None
    DOMAIN = 'conceptnet5.media.mit.edu'
    ENDPOINTROOT = '/data/5.4/'

    def __init__(self):
        self._client = MongoClient(rs.HOST)
        self.pool = HTTPConnectionPool(self.DOMAIN)
        # self.debugger = Debugger("H:\\PDS\\BNP\\insight_extraction\\ztests\\conceptnet\\", "conceptnet")

    def _form_url(self, word, lang):
        # TODO define rules to construct url
        url = self.ENDPOINTROOT + 'c/' + lang + '/' + word
        return url

    def get_concepts(self, word, lang):
        try:

            concepts = {}
            url = self._form_url(word, lang)

            # checking in cache
            cache = self.get_from_cache(url)
            if cache is None:
                r = self.pool.request('GET', url.encode('utf8'))
                data = json.loads(r.data)

                for edge in data['edges']:
                    if edge['rel'] in rs.CONCEPTNET_RELATION_MAPPING:
                        # self._log_relation(word, edge)  # log info
                        if edge['weight'] >= rs.CONCEPTNET_RELATION_MAPPING[edge['rel']]['threshold']:
                            key_concept = edge['end'] + "," + edge['rel']
                            if key_concept not in concepts:
                                concepts[key_concept] = {
                                    'end': edge['end'], 'rel': edge['rel'],
                                    'weight': edge['weight']
                                }
                            else:
                                if edge['weight'] > concepts[key_concept]['weight']:
                                    # replace relationship
                                    concepts[key_concept] = {
                                        'end': edge['end'], 'rel': edge['rel'],
                                        'weight': edge['weight']
                                    }

                    else:
                        if edge['rel'] not in rs.CONCEPTNET_RELATION_BAN_MAPPING:
                            # print "NEW RELATION FOUND!", edge['rel']
                            pass

                # store in cache
                self.store_in_cache(url, concepts)
                return concepts
            else:
                return cache
        except: 
            return None


    def get_from_cache(self, uri):
        o = self._client.cache.concepts.find_one({"_id": uri})
        if o and 'concepts' in o:
            return o['concepts']
        else:
            return None

    def store_in_cache(self, uri, concepts):
        self._client.cache.concepts.insert({"_id": uri, "concepts": concepts})


    def _log_relation(self, word, edge):
        conceptnetkeys = ['end', 'start', 'rel', 'weight', 'surfaceText']
        # self.debugger.set_header('conceptnet', ['word'] + conceptnetkeys)
        # loggmessage = word + self.debugger.SEP
        for key in conceptnetkeys:
            try:
                if isinstance(edge[key], unicode):
                    string = str(edge[key].encode('utf8'))
                else:
                    string = str(edge[key])
                # loggmessage += string + self.debugger.SEP
            except:
                e = sys.exc_info()[0]
                print ("ERROR %s", e)
                traceback.print_exc(file=sys.stdout)
                # loggmessage += "" + self.debugger.SEP
            # self.debugger.log("conceptnet", loggmessage)


# test remove
if __name__ == '__main__':
    startTime = datetime.now()
    api = ConceptNetAPI()
    for i in range(0, 100):
        c = api.get_concepts("toast", 'en')
        print len(c)
    print "done at", datetime.now() - startTime



