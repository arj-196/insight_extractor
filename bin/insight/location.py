import csv
from pymongo import MongoClient
import resources as rs
import hashlib as hl
import normalizer
from pattern.text.fr import ngrams
from datetime import datetime
startTime = datetime.now()


class LocationExtractor(object):
    normalizer = None
    iteration = 0

    def __init__(self, lang):
        self._client = MongoClient(rs.HOST)
        self.normalizer = normalizer.SimpleNormalizer(lang)

        # latlon list
        self.latlon = {}
        with open('../bin/location/city_lat_lon.csv', 'rb') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
            for row in spamreader:
                self.latlon[row[0].lower()] = {
                    'lat': row[1],
                    'lon': row[2]
                }

        # cities list
        self.cities = []
        with open('../bin/location/list_cities.csv', 'rb') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=',', quotechar='|')
            for row in spamreader:
                city = row[1].lower()
                self.cities.append(city)

        self.citiesnormalized = []
        for city in self.cities:
            self.citiesnormalized.append(
                self.normalizer.normalize_text(city, ifdecode=False)
            )


    def _extract_from_sentence(self, verbatim):
        norm_verbatim = self.normalizer.normalize_text(verbatim, ifdecode=False)
        freq = {}
        cache = {}
        for norm_city in self.citiesnormalized:
            token_city = tuple(norm_city.split(" "))
            length = len(token_city)

            if length not in cache:
                grams = ngrams(norm_verbatim, n=length)
                cache[length] = grams
            else:
                grams = cache[length]

            if token_city in grams:
                if norm_city not in freq:
                    freq[norm_city] = 1
                else:
                    freq[norm_city] += 1

        return freq

    def extract(self, topic, ifsave=True):
        print "topic : ", topic, datetime.now() - startTime
        topic_freq = {}
        for d in self._client.indexes.verbatims.find({"topic": topic}, no_cursor_timeout=True):
            freq = self._extract_from_sentence(d['verbatim'].encode('utf8'))

            for city in freq:
                if city not in topic_freq:
                    topic_freq[city] = freq[city]
                else:
                    topic_freq[city] += freq[city]

            self.iteration += 1
            if self.iteration % 5000 == 0:
                print "treated", str(self.iteration), "verbatims", datetime.now() - startTime

        output = []
        for city in topic_freq:
            if city in self.latlon:
                latlon = self.latlon[city]
                output.append({
                    'city': city,
                    'value': topic_freq[city],
                    'lat': latlon['lat'],
                    'lon': latlon['lon'],
                })

        print "found : ", str(len(output))
        print "-------"
        if ifsave:
            self._client.indexes.heatmaps.remove({'topic': topic})
            for d in output:
                d['_id'] = hl.md5(topic + d['city']).hexdigest()
                d['topic'] = topic
                self._client.indexes.heatmaps.insert(d)


