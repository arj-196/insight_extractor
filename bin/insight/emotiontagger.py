# -*- coding: utf-8 -*-
import resources as rs
from cache import CacheMap
from conjugate import CONJUGATER


class EmotionTagger(object):
    def __init__(self, lang):
        self.lang = lang
        self.WORDS = rs.EMOTIONS[lang]
        self.CONJUGATER = CONJUGATER(lang)
        self.cache = CacheMap()

    def tag(self, terms):
        emotions = {}
        list_words = [t.encode('utf8') for t in terms.keys()]
        for _type in self.WORDS.keys():
            if _type not in emotions:
                emotions[_type] = []

            for term in self.WORDS[_type]:
                grams = term.split(" ")
                if len(grams) == 1:

                    t = term.decode('utf8')
                    # get conjugations
                    if not self.cache.has_value(t):
                        conjugates = self.CONJUGATER.get_all_conjugates(t)
                        self.cache.set(t, conjugates)
                    else:
                        conjugates = self.cache.get(t)

                    for conjugate in conjugates:
                        if conjugate in list_words:
                            emotions[_type].append(conjugate)
        return emotions
