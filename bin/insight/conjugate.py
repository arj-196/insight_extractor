# -*- coding: utf-8 -*-
from pattern.text.fr import conjugate as conjugateFR
from pattern.text.fr import PRESENT, INFINITIVE, PAST
from pattern.text.fr import IMPERFECT, PRETERITE, SG, INDICATIVE, IMPERFECTIVE


class CONJUGATER(object):
    def __init__(self, lang):
        self.lang = lang

    def get_all_conjugates(self, term):
        list_conjugates = [term.encode('utf8')]
        if self.lang == 'en':
            return list_conjugates

        elif self.lang == 'fr':
            tenses = ["present", "past", "infinitive"]
            persons = [None, 1, 2, 3]
            numbers = [None, "SG", "PL"]
            moods = [None, "indicative", "imperative", "conditional", "subjunction"]
            aspects = [None, "imperfective", "perfective", "progressive"]
            negatives = [None, True, False]
            aliases = [
                "inf", "1sg", "2sg", "3sg", "1pl", "2pl", "3pl", "part",
                "2sg!", "1pl!", "2pl!",
                "1sg?", "2sg?", "3sg?", "1pl?", "2pl?", "3pl?",
                "1sgp", "2sgp", "3sgp", "1ppl", "2ppl", "3ppl", "ppart",
                "1sgp+", "2sgp+", "3sgp+", "1ppl+", "2ppl+", "3ppl+",
                "1sgp?", "2sgp?", "3sgp?", "1ppl?", "2ppl?", "3ppl?",
                "1sgf", "2sgf", "3sgf", "1plf", "2plf", "3plf",
            ]

            for tense in tenses:
                for person in persons:
                    for mood in moods:
                        for aspect in aspects:
                            # conjugate the term
                            c = conjugateFR(term, tense=tense, person=person, mood=mood, aspect=aspect)
                            if c:
                                c = c.encode('utf8')

                            if c and c not in list_conjugates:
                                list_conjugates.append(c)

                            # print tense, person, mood, aspect, " : ", c

            return list_conjugates


# conjugater = CONJUGATER('fr')
# l = conjugater.get_all_conjugates("être".decode('utf8'))
# print "-------------"
# print l
