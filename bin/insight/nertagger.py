import resources as rs
import supportapi


class NERTagger(object):
    def __init__(self, lang):
        self.lang = lang
        self.api = supportapi.SupportAPI()


    def tag(self, text):
        return self.api.get_ner(text)
