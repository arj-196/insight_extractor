# -*- coding: utf-8 -*-
import sys, traceback
import re
from pymongo import MongoClient
import resources as rs
from indexer import WordCloudIndexer
from datetime import datetime
import xlsxwriter
import json
import hashlib as hl

TYPETOCOLLECTIONMAP = {
    "text": "verbatimterms",
    "keyword": "verbatimkeywords",
    "concept": "verbatimconcepts",
    "tag": "verbatimtags",
    "domain": "verbatims",
}
MAXOUTPUTSIZE = 75


class Statistics(object):
    SAMPLESIZE = 100000

    def __init__(self):
        self.client = MongoClient(rs.HOST)


    def if_should_sample(self, topic):
        size = self.client.indexes.verbatims.count({'topic': topic})
        if size > self.SAMPLESIZE:
            return True
        else:
            return False

    def get_sample_size(self, topic):
        return self.SAMPLESIZE

    def get_id_from_or_statement(self, topic, searchmode, orStatement, languageMap):
        col = self.client.indexes[TYPETOCOLLECTIONMAP[searchmode]]
        if self.if_should_sample(topic):
            if orStatement:
                vids = [d['_id'] for d in col.aggregate(
                    [{'$match': {"topic": topic, "$or": orStatement}}, {'$sample': {'size': self.get_sample_size(topic)}},
                     {'$project': {'_id': 1}}])]
            else:
                vids = [d['_id'] for d in col.aggregate(
                    [{'$match': {"topic": topic}}, {'$sample': {'size': self.get_sample_size(topic)}},
                     {'$project': {'_id': 1}}])]
        else:
            if orStatement:
                vids = [d['_id'] for d in
                        col.find({"topic": topic, "$or": orStatement}, {'_id': 1}, no_cursor_timeout=True)]
            else:
                vids = [d['_id'] for d in col.find({"topic": topic}, {'_id': 1}, no_cursor_timeout=True)]

        return vids

    def normalize(self, _map):
        freq = [_map[k] for k in _map]
        if freq:
            _max = max(freq)
            _list = []
            for k in _map:
                if _map[k] > 0:
                    _list.append({
                        "name": k,
                        "frequency": _map[k] * 1.0 / _max
                    })
            return _list
        else:
            return []

    def normalize_list(self, _map):
        _list = []
        for k in _map:
            if _map[k] > 0:
                _list.append({
                    "name": k,
                    "frequency": _map[k]
                })
        return sorted(_list, key=lambda l: l['frequency'], reverse=True)[0:MAXOUTPUTSIZE]

    def _map_freq(self, _map, entries, _type):
        for entry in entries:
            if entry not in _map:
                _map[entry] = 0

            if _type is list:
                _map[entry] += len(entries[entry])
            elif _type is int:
                _map[entry] += entries[entry]
        return _map

    def _get_lang_for_topic(self, topic):
        lang = None
        settings = self.client.indexes.settings.find_one({"topic": topic})
        if settings or 'lang' in settings:
            lang = settings['lang']
        return lang


class GeneralStatistics(Statistics):
    def index(self, topic):
        print 'emotion statistics index', topic
        _map = {}
        for d in self.client.indexes.verbatims.find({"topic": topic}, no_cursor_timeout=True):
            _map = self._map_freq(_map, d['emotion'])
        # format dictionary
        _map = self._format_map(_map)
        return self.normalize(_map)

    def get_stats(self, searchmode, topic, orStatement, languageMap, predefinedVids=None):

        # gettings verbatim ids
        if predefinedVids is None:
            vids = self.get_id_from_or_statement(topic, searchmode, orStatement, languageMap)
        else:
            vids = predefinedVids

        _map_emotion = {}
        _map_hashtag = {}
        _map_mention = {}
        _map_domains = {}
        for d in self.client.indexes.verbatims.find({"_id": {"$in": vids}},
                                                    {'emotion': 1, 'hashtags': 1, 'mentions': 1, 'urls': 1},
                                                    no_cursor_timeout=True):
            if 'emotion' in d:
                _map_emotion = self._map_freq(_map_emotion, d['emotion'], list)
            if 'hashtags' in d:
                _map_hashtag = self._map_freq(_map_hashtag, d['hashtags'], int)
            if 'mentions' in d:
                _map_mention = self._map_freq(_map_mention, d['mentions'], int)
            if 'urls' in d and 'domains' in d['urls']:
                _domains = {}
                for dm in d['urls']['domains']:
                    _domains[dm['_id']] = dm['value']
                _map_domains = self._map_freq(_map_domains, _domains, int)

        # format dictionary
        _map_emotion = self._format_map(_map_emotion)
        return {
            'emotions': self.normalize(_map_emotion),
            'hashtags': self.normalize_list(_map_hashtag),
            'mentions': self.normalize_list(_map_mention),
            'domains': self.normalize_list(_map_domains)
        }

    def _format_map(self, _map):
        rejects = [
            u'Coupure avec ses émotions'
        ]
        for k in rejects:
            if k in _map:
                del _map[k]
        return _map


class NERStatistics(Statistics):
    MAXENTRIES = 30

    def get_stats(self, searchmode, topic, orStatement, languageMap, predefinedVids=None):
        # gettings verbatim ids
        if predefinedVids is None:
            vids = self.get_id_from_or_statement(topic, searchmode, orStatement, languageMap)
        else:
            vids = predefinedVids

        col = self.client.indexes.verbatimners
        vs = [d for d in col.find({"_id": {"$in": vids}}, {'_id': 1, 'entities': 1}, no_cursor_timeout=True)]

        _map = {}
        for d in vs:
            for et in d['entities']:
                if et not in _map:
                    _map[et] = {}
                for e in d['entities'][et]:
                    if e not in _map[et]:
                        _map[et][e] = d['entities'][et][e]
                    else:
                        _map[et][e] += d['entities'][et][e]
        freq = {}
        for t in _map:
            freq[t] = sorted(self.normalize(_map[t]), key=lambda o: o['frequency'], reverse=True)[0:self.MAXENTRIES]
        return freq

    def normalize(self, _map):
        _list = []
        for k in _map:
            if _map[k] > 0:
                _list.append({
                    "name": k,
                    "frequency": _map[k]
                })
        return _list


class WordcloudStatistics(Statistics):
    MAXWORDS = 300

    def get_stats(self, searchmode, targetmode, topic, orStatement, languageMap, predefinedVids=None):
        # get lang
        lang = self._get_lang_for_topic(topic)

        # gettings verbatim ids
        if predefinedVids is None:
            vids = self.get_id_from_or_statement(topic, searchmode, orStatement, languageMap)
        else:
            vids = predefinedVids

        def filternone(c, lang):
            return False

        if targetmode == "text":
            filtermethod = WordCloudIndexer.if_delete_term
            target = "terms"
            items = [d for d in self.client.indexes.verbatimterms.find(
                {"_id": {"$in": vids}}, {"_id": -1, target: 1}, no_cursor_timeout=True)]
        elif targetmode == "keyword":
            filtermethod = filternone
            target = "keywords"
            items = [d for d in
                     self.client.indexes.verbatimkeywords.find(
                         {"_id": {"$in": vids}}, {"_id": -1, target: 1}, no_cursor_timeout=True)]
        elif targetmode == "concept":
            filtermethod = filternone
            target = "concepts"
            items = [d for d in
                     self.client.indexes.verbatimconcepts.find(
                         {"_id": {"$in": vids}}, {"_id": -1, target: 1}, no_cursor_timeout=True)]
        else:
            return {"r": [], "c": 0}

        _map = {}
        for item in items:
            for w in item[target]:
                if not filtermethod(w, lang):
                    if w not in _map:
                        _map[w] = 1
                    else:
                        _map[w] += 1

        # map to list
        _list = [(w, _map[w]) for w in _map]
        del _map
        _list = sorted(_list, key=lambda t: t[1], reverse=True)

        count = len(_list)
        _list = _list[0:self.MAXWORDS]

        r = {"r": [{"_id": w[0], "value": w[1]} for w in _list], "c": count}
        return r


class SearchEngineStatistics(Statistics):
    def __init__(self):
        self.generalStats = GeneralStatistics()
        self.wordcloudStats = WordcloudStatistics()
        self.nerStats = NERStatistics()
        super(SearchEngineStatistics, self).__init__()

    def get_id_from_or_statement_and_vids(self, topic, searchmode, orStatement, languageMap, vids):
        col = self.client.indexes[TYPETOCOLLECTIONMAP[searchmode]]

        if vids:
            vids = [d['_id'] for d in
                    col.find({'_id': {'$in': vids}, "topic": topic, "$or": orStatement}, {'_id': 1},
                             no_cursor_timeout=True)]
        else:
            vids = [d['_id'] for d in
                    col.find({"topic": topic, "$or": orStatement}, {'_id': 1}, no_cursor_timeout=True)]

        return vids

    def doSearch(self, sessionid, topic):
        # get searchList
        session = self.client.client.sessions.find_one({"_id": sessionid})
        if 'searchList' in session:
            searchList = json.loads(session['searchList'])
            vids = []
            for i, sm in enumerate(searchList):
                if i > 0 and not vids:
                    break
                vids = self.get_id_from_or_statement_and_vids(topic, sm['s'], sm['o'], {}, vids)

            # getting statistics
            # TODO do a iterative storing solution incase the document is very large!
            responseid = hl.md5(sessionid + str(datetime.now())).hexdigest()
            r = {
                "_id": responseid,
                "sessionid": sessionid,
                "count": len(vids),
                "vids": vids[:14],  # Storing only 15 as there is no itertive solution at the moment
                "general": self.generalStats.get_stats(None, topic, None, None, predefinedVids=vids),
                "wordcloud": {
                    "keyword": self.wordcloudStats.get_stats(None, "keyword", topic, None, None, predefinedVids=vids),
                    "text": self.wordcloudStats.get_stats(None, "text", topic, None, None, predefinedVids=vids),
                    "concept": self.wordcloudStats.get_stats(None, "concept", topic, None, None, predefinedVids=vids),
                },
                "ner": self.nerStats.get_stats(None, topic, None, None, predefinedVids=vids)
            }

            self.client.client.aggregate.insert(r)
            return responseid

        else:
            # no searchList.
            return None


class ExportStatistics(Statistics):
    ptnconcept = re.compile("/c/[a-z]*/(\w*)")

    def __init__(self):
        super(ExportStatistics, self).__init__()
        self.generalStats = GeneralStatistics()
        self.wordcloudStats = WordcloudStatistics()

    def export(self, _dir, searchmode, topic, orStatement, languageMap):
        # gettings verbatim ids
        if len(orStatement) > 0:
            vids = self.get_id_from_or_statement(topic, searchmode, orStatement, languageMap)
        else:
            vids = [d['_id'] for d in self.client.indexes.verbatims.find({"topic": topic}, {"_id": 1})]

        # get verbatims
        vs = [d for d in self.client.indexes.verbatims.find({"_id": {"$in": vids}}, no_cursor_timeout=True)]

        # get keywords
        ks = [d for d in self.client.indexes.verbatimkeywords.find({"_id": {"$in": vids}}, no_cursor_timeout=True)]

        # get concepts
        cs = [d for d in self.client.indexes.verbatimconcepts.find({"_id": {"$in": vids}}, no_cursor_timeout=True)]

        # get general stats
        general = self.generalStats.get_stats(searchmode, topic, orStatement, languageMap)

        # get wordcloud stats
        targetmodes = ["text", "keyword", "concept"]
        wordclouds = {}
        for tm in targetmodes:
            wordclouds[tm] = self.wordcloudStats.get_stats(searchmode, tm, topic, orStatement, languageMap)

        # filename
        filename = "export_" + topic + "_" + searchmode + "_" + datetime.now().strftime("%d_%m_%Y.%H_%M_%S.")
        return self._export_to_file("excel", _dir, filename, vs, ks, cs, general, wordclouds)

    def _export_to_file(self, _type, _dir, filename, vs, ks, cs, general, wcs):
        """
        formatting all components for writing to file
        :param _type:
        :param filename:
        :param vs:
        :param ks:
        :param cs:
        :param general:
        :param wcs:
        :return:
        """

        # verbatims
        verbatims = []
        for v, k, c in zip(vs, ks, cs):
            verbatims.append({
                "v": self._encode(v['verbatim']),
                "k": " ".join([k for k in k['keywords']]),
                "c": " ".join([self._parse_concept(c) for c in c['concepts']]),
            })

        # wordclouds
        wordclouds = {}
        for twc in wcs:
            wordclouds[twc] = {}
            D = wcs[twc]
            wordclouds[twc]['total'] = D['c']
            wordclouds[twc]['list'] = []
            for w in D['r']:
                wordclouds[twc]['list'].append({
                    'name': self._encode(w['_id']),
                    'frequency': w['value']
                })

        if _type == "excel":
            return self._export_to_excel(_dir, filename, verbatims, general, wordclouds)

    def _export_to_excel(self, _dir, filename, verbatims, general, wordclouds):
        """
        Exporting to EXCEL file
        :param filename:
        :param verbatims:
        :param general:
        :param wordclouds:
        :return:
        """
        # create workbook
        filename += "xlsx"
        workbook = xlsxwriter.Workbook(filename=_dir + filename)

        # format objects
        bold = workbook.add_format({'bold': True})

        #
        # general stats
        worksheetgeneral = workbook.add_worksheet(name="general_statistics")
        worksheetgeneral.write('A1', 'Emotions', bold)
        worksheetgeneral.write('A3', 'Emotion', bold)
        worksheetgeneral.write('B3', 'Ratio Index', bold)  # TODO verify index name

        # emotions
        row = 3
        col = 0
        for k in general['emotions']:
            worksheetgeneral.write(row, col, k['name'])
            worksheetgeneral.write(row, col + 1, k['frequency'])
            row += 1

        #
        # draw emotion chart
        chart = workbook.add_chart({'type': 'column'})
        chart.add_series({
            'categories': '=general_statistics!$A$4:$A$' + str(4 + (len(general['emotions']) - 1)),
            'values': '=general_statistics!$B$4:$B$' + str(4 + (len(general['emotions']) - 1)),
            'name': "Emotions"
        })
        worksheetgeneral.insert_chart('D3', chart)

        #
        # verbatims
        worksheet = workbook.add_worksheet(name="verbatims")
        worksheet.write('A1', 'Verbatims', bold)
        worksheet.write('B1', 'Concepts', bold)
        worksheet.write('C1', 'Keywords', bold)

        # Write a total using a formula.
        worksheet.write(1, 0, 'Total Verbatims', bold)
        worksheet.write(1, 1, str(len(verbatims)))

        row = 3
        col = 0
        for v in verbatims:
            worksheet.write(row, col, v['v'])
            worksheet.write(row, col + 1, v['c'])
            worksheet.write(row, col + 2, v['k'])
            row += 1

        #
        # wordclouds
        worksheet = workbook.add_worksheet(name="wordclouds")
        worksheet.write('A1', 'Keywords', bold)
        worksheet.write('A2', 'Total', bold)
        worksheet.write('B2', wordclouds['keyword']['total'], bold)
        worksheet.write('A4', 'word', bold)
        worksheet.write('B4', 'frequency', bold)

        worksheet.write('D1', 'Terms', bold)
        worksheet.write('D2', 'Total', bold)
        worksheet.write('E2', wordclouds['text']['total'], bold)
        worksheet.write('D4', 'word', bold)
        worksheet.write('E4', 'frequency', bold)

        worksheet.write('G1', 'Concepts', bold)
        worksheet.write('G2', 'Total', bold)
        worksheet.write('H2', wordclouds['concept']['total'], bold)
        worksheet.write('G4', 'word', bold)
        worksheet.write('H4', 'frequency', bold)

        row = 4
        col = 0
        # for t in wc_types:
        for k in wordclouds['keyword']['list']:
            worksheet.write(row, col, k['name'])
            worksheet.write(row, col + 1, k['frequency'])
            row += 1

        row = 4
        col = 3
        # for t in wc_types:
        for k in wordclouds['text']['list']:
            worksheet.write(row, col, k['name'])
            worksheet.write(row, col + 1, k['frequency'])
            row += 1

        row = 4
        col = 6
        # for t in wc_types:
        for k in wordclouds['concept']['list']:
            worksheet.write(row, col, self._parse_concept(k['name']))
            worksheet.write(row, col + 1, k['frequency'])
            row += 1

        #
        # draw wordcloud chart
        worksheetgeneral.write('A' + str(4 + (len(general['emotions']) + 10)), 'Wordcloud Charts', bold)
        # keywords
        chart = workbook.add_chart({'type': 'column'})
        chart.add_series({
            'categories': '=wordclouds!$A$5:$A$' + str(5 + (len(wordclouds['keyword']['list']) - 1)),
            'values': '=wordclouds!$B$5:$B$' + str(5 + (len(wordclouds['keyword']['list']) - 1)),
            'name': "Keywords"
        })
        worksheetgeneral.insert_chart('A' + str(4 + (len(general['emotions']) + 12)), chart)
        # terms
        chart = workbook.add_chart({'type': 'column'})
        chart.add_series({
            'categories': '=wordclouds!$D$5:$D$' + str(5 + (len(wordclouds['text']['list']) - 1)),
            'values': '=wordclouds!$E$5:$E$' + str(5 + (len(wordclouds['text']['list']) - 1)),
            'name': "Terms"
        })
        worksheetgeneral.insert_chart('J' + str(4 + (len(general['emotions']) + 12)), chart)
        # concepts
        chart = workbook.add_chart({'type': 'column'})
        chart.add_series({
            'categories': '=wordclouds!$G$5:$G$' + str(5 + (len(wordclouds['concept']['list']) - 1)),
            'values': '=wordclouds!$H$5:$H$' + str(5 + (len(wordclouds['concept']['list']) - 1)),
            'name': "Concepts"
        })
        worksheetgeneral.insert_chart('S' + str(4 + (len(general['emotions']) + 12)), chart)

        #
        # meta data
        worksheet = workbook.add_worksheet(name="meta_data")
        worksheet.write('A1', 'HashTags', bold)
        worksheet.write('A2', 'Total', bold)
        worksheet.write('B2', len(general['hashtags']), bold)
        worksheet.write('A4', 'hashtag', bold)
        worksheet.write('B4', 'frequency', bold)

        worksheet.write('D1', 'Mentions', bold)
        worksheet.write('D2', 'Total', bold)
        worksheet.write('E2', len(general['mentions']), bold)
        worksheet.write('D4', 'mention', bold)
        worksheet.write('E4', 'frequency', bold)

        worksheet.write('G1', 'Domains', bold)
        worksheet.write('G2', 'Total', bold)
        worksheet.write('H2', len(general['domains']), bold)
        worksheet.write('G4', 'word', bold)
        worksheet.write('H4', 'frequency', bold)

        row = 4
        col = 0
        # for t in wc_types:
        for k in general['hashtags']:
            worksheet.write(row, col, k['name'])
            worksheet.write(row, col + 1, k['frequency'])
            row += 1

        row = 4
        col = 3
        # for t in wc_types:
        for k in general['mentions']:
            worksheet.write(row, col, k['name'])
            worksheet.write(row, col + 1, k['frequency'])
            row += 1

        row = 4
        col = 6
        # for t in wc_types:
        for k in general['domains']:
            worksheet.write(row, col, k['name'])
            worksheet.write(row, col + 1, k['frequency'])
            row += 1

        #
        # draw metadata chart
        worksheetgeneral.write('A' + str(4 + (len(general['emotions']) + 30)), 'Meta Data Charts', bold)
        # hashtags
        chart = workbook.add_chart({'type': 'column'})
        chart.add_series({
            'categories': '=meta_data!$A$5:$A$' + str(5 + (len(general['hashtags']) - 1)),
            'values': '=meta_data!$B$5:$B$' + str(5 + (len(general['hashtags']) - 1)),
            'name': "Hashtags"
        })
        worksheetgeneral.insert_chart('A' + str(4 + (len(general['emotions']) + 32)), chart)
        # mentions
        chart = workbook.add_chart({'type': 'column'})
        chart.add_series({
            'categories': '=meta_data!$D$5:$D$' + str(5 + (len(general['mentions']) - 1)),
            'values': '=meta_data!$E$5:$E$' + str(5 + (len(general['mentions']) - 1)),
            'name': "Mentions"
        })
        worksheetgeneral.insert_chart('J' + str(4 + (len(general['emotions']) + 32)), chart)
        # domains
        chart = workbook.add_chart({'type': 'column'})
        chart.add_series({
            'categories': '=meta_data!$G$5:$G$' + str(5 + (len(general['domains']) - 1)),
            'values': '=meta_data!$H$5:$H$' + str(5 + (len(general['domains']) - 1)),
            'name': "Domains"
        })
        worksheetgeneral.insert_chart('S' + str(4 + (len(general['emotions']) + 32)), chart)

        # finally close
        workbook.close()

        return filename

    def _parse_concept(self, c):
        r = re.findall(self.ptnconcept, c)
        if r and len(r) > 0:
            return r[0]
        else:
            return c

    def _encode(self, _str, encoding='utf8', errormessage=True):
        return _str
        # try:
        #     _str.encode('ascii')
        #     try:
        #         return _str.encode(encoding)
        #     except:
        #         return _str
        # except:
        #     if errormessage:
        #         return "Encoding Not Supported"
        #     else:
        #         return ""
