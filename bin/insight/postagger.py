# -*- coding: utf-8 -*-
from pattern.text.en import tag as tagEN
from pattern.text.fr import tag as tagFR
from pattern.text.en import singularize as singularizeEN
from pattern.text.fr import singularize as singularizeFR
from pattern.text.en import conjugate as conjugateEN
from pattern.text.fr import conjugate as conjugateFR
from pattern.text.en import INFINITIVE as infinitiveEN
from pattern.text.fr import INFINITIVE as infinitiveFR
from resources import tagset as TAGSET


class PatternPOSTagger(object):
    def __init__(self, lang):
        self.lang = lang

    def tosingle(self, word):
        singularize = None
        if self.lang == 'en':
            singularize = singularizeEN
        elif self.lang == 'fr':
            singularize = singularizeFR

        if singularize:
            return singularize(word)
        else:
            print "ERR: cannot handle language"
            return word

    def toinfinitive(self, word):
        conjugate = None
        infinitive = None
        if self.lang == 'en':
            conjugate = conjugateEN
            infinitive = infinitiveEN
        elif self.lang == 'fr':
            conjugate = conjugateFR
            infinitive = infinitiveFR

        if conjugate:
            return conjugate(word, infinitive)
        else:
            print "ERR: cannot handle language"
            return word

    def _process(self, word, action):
        if action == 'toSingle':
            r = self.tosingle(word)
        elif action == 'toInfinitive':
            r = self.toinfinitive(word)
        else:
            print "ERR: action not recognized"
            r = word

        # print "prcess", word, action, p  # test statement
        return r

    # TODO use tag on terms or use parsetree ?
    def tagterms(self, terms, text):
        tagger = None
        if self.lang == 'en':
            tagger = tagEN
        elif self.lang == 'fr':
            tagger = tagFR

        tagged = {}
        for term in terms:
            count = terms[term]
            pos = tagger(term)
            o = {
                "term": term,
                "count": count
            }

            # if consider this pos
            if pos[0][1] in TAGSET:
                paction = TAGSET[pos[0][1]]

                # if prossesing required
                if paction['action']:
                    o['term'] = self._process(term, paction['action'])

                if paction['tag'] not in tagged:
                    tagged[paction['tag']] = [o]
                else:
                    tagged[paction['tag']].append(o)

        return tagged
