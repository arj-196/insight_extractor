# -*- coding: utf-8 -*-
from pattern.vector import words
from nltk.tokenize import TweetTokenizer
from pattern.text.en import tokenize as tokenizeEN
from pattern.text.fr import tokenize as tokenizeFR


class PatternTokeniser(object):
    def __init__(self, lang):
        self.tknzr = TweetTokenizer()
        self.lang = lang

    @staticmethod
    def tokenize(text):
        return words(text)

    def tokenize_sentence(self, text):
        if self.lang == 'en':
            return tokenizeEN(text)
        elif self.lang == 'fr':
            return tokenizeFR(text)
        else:
            raise Exception("Cannot handle lang: ", self.lang)
