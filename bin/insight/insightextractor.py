# -*- coding: utf-8 -*-
import tokeniser
import normalizer
import postagger
import emotiontagger
import nertagger
import sentiment as sentimentTagger
import hashlib as hl
from pattern.vector import Document, words, count
from langdetect import detect_langs
from conceptapi import ConceptNetWebAPI
import taggers
import indexer as INDEXER
from bin.file import reader
import json
import os


class InsightExtractorManager(object):
    ifsave = True
    ifconcept = True
    ifkeyword = True
    ifpos = True
    ifsentiment = True
    ifterm = True
    ifemotion = True
    ifNER = False
    ifHashTags = True
    ifMentions = True
    ifUrl = True

    def __init__(self, inputfile, topic, project, company, user, lang, extractor):
        self.file = inputfile
        self.topic = topic
        self.project = project
        self.company = company
        self.user = user
        self.lang = lang
        self.extractor = extractor

    def preview(self, dumpdir):
        """
        Extracts first 5 Verbatims, and stores in a dump file
        :param dumpdir:
        :return:
        """
        content = reader.FileReader(open(self.file, 'rb')).read()
        verbatims = []
        extractor = self.extractor(lang=self.lang)
        for i, line in enumerate(content):
            if i < 5:
                try:
                    string = line
                    c = extractor.extract(
                        string, self.topic,
                        self.project, self.user, self.company,
                        prefetcheddata=None,
                        ifsave=False,
                        ifconcept=False, ifkeyword=self.ifkeyword,
                        ifpos=False, ifsentiment=self.ifsentiment, ifterm=False,
                        ifemotion=False, ifNER=False,
                        ifHashTags=False, ifMentions=False,
                        ifUrl=False,
                    )
                    verbatims.append(c)
                except:
                    pass

        # finally store in pickle
        f = self.file.split('/')
        filedump = dumpdir + f[len(f) - 1] + '.json'

        print os.path.dirname(os.path.realpath(__file__))

        json.dump({
            'project': self.project,
            'topic': self.topic,
            'company': self.company,
            'user': self.user,
            'lang': self.lang,
            'count': len(content),
            'verbatims': verbatims,
        }, open(filedump, 'wb'), encoding='UTF-8')

        return os.path.abspath(filedump)

    def extract(self):
        """
        Extract all Verbatims, and store in DB
        :return:
        """

        content = reader.FileReader(open(self.file, 'rb')).read()
        extractor = self.extractor(lang=self.lang)
        for line in content:
            try:
                string = line
                extractor.extract(
                    string, self.topic,
                    self.project, self.user, self.company,
                    prefetcheddata=None,
                    ifsave=self.ifsave,
                    ifconcept=self.ifconcept, ifkeyword=self.ifkeyword,
                    ifpos=self.ifpos, ifsentiment=self.ifsentiment, ifterm=self.ifterm,
                    ifemotion=self.ifemotion, ifNER=self.ifNER,
                    ifHashTags=self.ifHashTags, ifMentions=self.ifMentions,
                    ifUrl=self.ifUrl,
                )
            except:
                pass

        # finally index the topic
        indexer = INDEXER.WordCloudIndexer()
        indexer.index(self.topic)



class InsightExtractor(object):
    tokenizer = None
    lang = None
    normalizer = None
    indexer = None
    conceptapi = None
    postagger = None
    emotiontagger = None
    nertagger = None
    sentimenttagger = None
    hashtagtagger = None
    mentiontagger = None
    urltagger = None
    THRESHOLD_KEYWORD = 0

    def __init__(self, lang='en', indexer=INDEXER.InsightIndexer()):
        self.lang = lang
        self.indexer = indexer

    def _clean_text(self, text):
        return self.normalizer.normalize_text(text)

    def _clean_tokens(self, tokens):
        return self.normalizer.clean_tokens(tokens)

    def _tokenize(self, text):
        return self.tokenizer.tokenize(text)

    def _tokenize_sentence(self, text):
        return self.tokenizer.tokenize_sentence(text)

    def _clean_keywords(self, keywords):
        return self.normalizer.clean_keywords(keywords)

    def extract(self, text, topic, project, user, company, prefetcheddata=None, ifconcept=False, ifsave=False,
                ifkeyword=True,
                ifpos=True, ifsentiment=True, ifterm=True, ifemotion=True, ifNER=True,
                ifHashTags=True, ifMentions=True, ifUrl=True):
        # normalize text
        text_normalized = self._clean_text(text)
        # create document
        d = Document(text_normalized, threshold=self.THRESHOLD_KEYWORD, language=self.lang)
        # # TODO enhancement, use Document to extract more relevant keywords
        # extract keywords from text
        if ifkeyword:
            keywords = self._clean_keywords(d.keywords())
        else:
            keywords = None

        # detect languages
        # detected_lang = self.detect_languages(text_normalized)
        detected_lang = []

        # extract concepts
        if ifconcept:
            # tokens = self._clean_tokens(self._tokenize(text_normalized))
            concepts = self.extract_concepts(detected_lang,
                                             keywords)  # TODO use keywords or tokens to extract concepts ?
        else:
            concepts = None

        # get sentiment
        if ifsentiment:
            sentiment = self.get_sentiment(text_normalized)
        else:
            sentiment = None

        # get post tags
        if ifpos:
            pos = self.postagger.tagterms(d.terms, text_normalized)
        else:
            pos = None

        # get terms
        if ifterm:
            terms = count(words(text))
        else:
            terms = None

        # get emotions
        if ifemotion:
            emotions = self.emotiontagger.tag(d.terms)
        else:
            emotions = None

        # get ners
        if ifNER:
            entities = self.nertagger.tag(text)
        else:
            entities = None

        # get hashtags
        if ifHashTags:
            hashtags = self.hashtagtagger.tag(text)
            # adding hashtags to terms
            if terms:
                for ht in hashtags:
                    terms[ht] = hashtags[ht]
        else:
            hashtags = None

        if ifMentions:
            mentions = self.mentiontagger.tag(text)
            # adding mentions to terms
            if terms:
                for m in mentions:
                    terms[m] = mentions[m]
        else:
            mentions = None

        if ifUrl:
            urls = self.urltagger.tag(text)
        else:
            urls = None

        # appending prefetched data
        formatted_prefetched_data = None
        if prefetcheddata:
            for key in prefetcheddata:
                if key != 'additional':
                    # manually defining how to handle different types
                    if key == 'domain':
                        if urls:
                            for newdomain in prefetcheddata['domain']['domains']:
                                urls['domains'].append({'_id': newdomain, 'value': 1})
                else:
                    formatted_prefetched_data = prefetcheddata['additional']

        if ifsave:
            # index extracted data
            self.indexer.index(
                text_normalized,
                text,
                topic,
                project,
                user,
                company,
                keywords,
                concepts,
                pos,
                terms,
                sentiment,
                hashtags,
                mentions,
                urls,
                emotions,
                entities,
                self.lang,
                prefetcheddata=formatted_prefetched_data,
            )

        return {
            'text_normalized': text_normalized,
            'text': text,
            'keywords': keywords,
            'concepts': concepts,
            'pos': pos,
            'terms': terms,
            'sentiment': sentiment,
            'hashtags': hashtags,
            'mentions': mentions,
            'urls': urls,
            'emotions': emotions,
            'entities': entities,
            'prefected_data': formatted_prefetched_data,
        }

    def detect_languages(self, text):
        return detect_langs(text)

    def extract_concepts(self, detected_lang, keywords):
        concepts = {}
        if self.lang in ['en', 'fr']:
            for keyword in keywords:
                if type(keyword) is tuple:
                    concepts[keyword[1]] = {
                        'id': hl.md5(keyword[1].encode('utf8')).hexdigest(),
                        'relevance': keyword[0],
                        'keyword': keyword[1],
                        'concepts': self.conceptapi.get_concepts(keyword[1], self.lang)
                    }
                else:
                    concepts[keyword] = {
                        'id': hl.md5(keyword.encode('utf8')).hexdigest(),
                        'relevance': 0,
                        'keyword': keyword,
                        'concepts': self.conceptapi.get_concepts(keyword, self.lang)
                    }

            return concepts
        else:
            raise Exception('cannot handle this language')

    def get_sentiment(self, text):
        return self.sentimenttagger.tag(text)


class BaseInsightExtactor(InsightExtractor):
    def __init__(self, lang):
        super(BaseInsightExtactor, self).__init__(lang)
        self.tokenizer = tokeniser.PatternTokeniser(lang)
        self.normalizer = normalizer.SimpleNormalizer(lang)
        self.conceptapi = ConceptNetWebAPI()
        self.postagger = postagger.PatternPOSTagger(lang)
        self.emotiontagger = emotiontagger.EmotionTagger(lang)
        self.nertagger = nertagger.NERTagger(lang)
        self.sentimenttagger = sentimentTagger.SentimentTagger(lang)
        self.hashtagtagger = taggers.HashtagTagger(lang)
        self.mentiontagger = taggers.MentionTagger(lang)
        self.urltagger = taggers.URLTagger(lang)
