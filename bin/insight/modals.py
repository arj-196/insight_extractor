# -*- coding: utf-8 -*-
from pymongo import MongoClient, DESCENDING, ASCENDING
import resources as rs
import hashlib as hl


class Modal(object):
    EXTRACTION_STRATEGY = {
        1: ['keywords', 'concepts', 'hashtags', 'mentions'],
        2: ['LOCATION', 'ORGANIZATION'],
        3: ['domains']
    }

    def __init__(self):
        self.client = MongoClient(rs.HOST)

    def get_verbatim_ids(self, insight):
        projecttopics = [d['topic'] for d in self.client.indexes.settings.find({'project': insight['project']})]

        return [d['_id'] for d in
                self.client.indexes.verbatimtags.find(
                    {'topic': {'$in': projecttopics}, 'tags': {'$in': insight['tags']}}, no_cursor_timeout=True)]

    def _extract_feature_list(self, vids, col, target):
        docs = [d for d in
                col.find({'_id': {'$in': vids}}, no_cursor_timeout=True)]  # TODO optimize query by projection
        _map = {}

        for doc in docs:
            targetvalue = None
            if doc:
                if target in self.EXTRACTION_STRATEGY[3]:
                    # extraction strategy 3
                    if 'urls' in doc and target in doc['urls']:
                        targetvalue = doc['urls'][target]

                    if targetvalue:
                        for item in targetvalue:
                            if item['_id'] not in _map:
                                _map[item['_id']] = 1
                            else:
                                _map[item['_id']] += 1

                else:
                    # extraction strategy 1
                    if target in self.EXTRACTION_STRATEGY[1]:
                        if target in doc:
                            targetvalue = doc[target]

                    # extraction strategy 2
                    elif target in self.EXTRACTION_STRATEGY[2]:
                        if 'entities' in doc and target in doc['entities']:
                            targetvalue = doc['entities'][target]

                    if targetvalue:
                        for item in targetvalue:
                            if item not in _map:
                                _map[item] = 1
                            else:
                                _map[item] += 1

        _list = [{'type': target, 'value': w, 'freq': _map[w]} for w in _map]
        sorted_list = sorted(_list, key=lambda k: k['freq'], reverse=True)
        return sorted_list

    def get_total_feature_count(self, feature_types):
        total = 0
        for features in feature_types:
            for feature in features:
                total += feature['freq']
        return total

    def get_feature_weights(self, feature_types, totalN):
        for i, features in enumerate(feature_types):
            for j, feature in enumerate(features):
                feature['weight'] = 1.0 * feature['freq'] / totalN
                feature_types[i][j] = feature

        return feature_types

    def hash_feature(self, insight, feature):
        raw_text = insight['_id'] + feature['type'] + feature['value']
        return self._hash(raw_text)

    def hash_prediction(self, insight, prediction):
        raw_text = insight['_id'] + prediction[0]
        return self._hash(raw_text)

    def hash_setting(self, insight, topic):
        raw_text = insight['_id'] + topic
        return self._hash(raw_text)

    def _hash(self, raw_text):
        try:
            text = raw_text.encode('utf8')
            return hl.md5(text).hexdigest()
        except:
            try:
                return hl.md5(raw_text).hexdigest()
            except:
                return None

    def remove_modal(self, insight):
        self.client.modal.modals.remove({'insightid': insight['_id']})

    def save_modal(self, insight, features):
        # remove previous modal
        self.remove_modal(insight)

        for featureList in features:
            for feature in featureList:
                id_feature = self.hash_feature(insight, feature)
                if id_feature:
                    try:
                        self.client.modal.modals.insert({
                            '_id': id_feature,
                            'insightid': insight['_id'],
                            'feature': feature,
                            'project': insight['project']
                        })
                    except:
                        pass

    def remove_predictions(self, insight):
        self.client.modal.predictions.remove({'insightid': insight['_id']})

    def store_predictions(self, insight, topic, docs):
        # remove previous predictions
        self.remove_predictions(insight)
        for doc in docs:
            if doc[1] > 0:
                id_prediction = self.hash_prediction(insight, doc)
                try:
                    self.client.modal.predictions.insert({
                        '_id': id_prediction,
                        'insightid': insight['_id'],
                        'vid': doc[0],
                        'score': doc[1],
                        'topic': topic,
                    })
                except:
                    pass


class AutomatedInsightModal(Modal):

    MAXVERBATIMS = 100
    TARGETFEATURES = ['keywords', 'concepts', 'hashtags', 'mentions', 'LOCATION', 'ORGANIZATION', 'domains']
    FEATURECOLLECTIONMAP = {
        'keywords': {'db': 'indexes', 'col': 'verbatimkeywords'},
        'concepts': {'db': 'indexes', 'col': 'verbatimconcepts'},
        'hashtags': {'db': 'indexes', 'col': 'verbatimterms'},
        'mentions': {'db': 'indexes', 'col': 'verbatimterms'},
        'LOCATION': {'db': 'indexes', 'col': 'verbatimterms'},
        'ORGANIZATION': {'db': 'indexes', 'col': 'verbatimterms'},
        'domains': {'db': 'indexes', 'col': 'verbatims'},
    }

    def train_on(self, insight):
        vids = self.get_verbatim_ids(insight)
        features = self.extract_features(vids)
        totalN = self.get_total_feature_count(features)
        features = self.get_feature_weights(features, totalN)
        self.save_modal(insight, features)

    def extract_features(self, vids, returntype=list):

        # keywords
        keywords = self._extract_feature_list(vids, self.client.indexes.verbatimkeywords, 'keywords')
        # concepts
        concepts = self._extract_feature_list(vids, self.client.indexes.verbatimconcepts, 'concepts')
        # hashtags
        hashtags = self._extract_feature_list(vids, self.client.indexes.verbatims, 'hashtags')
        # mentions
        mentions = self._extract_feature_list(vids, self.client.indexes.verbatims, 'mentions')
        # locations
        locations = self._extract_feature_list(vids, self.client.indexes.verbatimners, 'LOCATION')
        # organizations
        organizations = self._extract_feature_list(vids, self.client.indexes.verbatimners, 'ORGANIZATION')
        # domains
        domains = self._extract_feature_list(vids, self.client.indexes.verbatims, 'domains')

        # list of all features
        return [keywords, concepts, hashtags, mentions, locations, organizations, domains]

    def _create_feature_search_query(self, top_features, topic, taggedvids):
        searchqueryentries = {}
        # iterating through features to construct search queries
        for feature in top_features:
            if feature['type'] not in searchqueryentries:
                searchqueryentries[feature['type']] = []

            if feature['value'] not in searchqueryentries[feature['type']]:
                searchqueryentries[feature['type']].append(feature['value'])

        # formatting searchquery
        searchqueries = []
        for _type in searchqueryentries:
            if _type == "keywords" or _type == "concepts":
                searchqueries.append((_type, {
                    'topic': topic,
                    _type: {'$in': searchqueryentries[_type]},
                    '_id': {'$nin': taggedvids},
                }))
            elif _type == "hashtags" or _type == "mentions" or _type == "LOCATION" or _type == "ORGANIZATION":
                searchqueries.append((_type, {
                    'topic': topic,
                    "terms": {'$in': searchqueryentries[_type]},
                    '_id': {'$nin': taggedvids},
                }))
            elif _type == "domains":
                searchqueries.append((_type, {
                    'topic': topic,
                    "domainList": {'$in': searchqueryentries[_type]},
                    '_id': {'$nin': taggedvids},
                }))


        # TODO complete : Problem with hashtags and mentions : Need to use aggregate

        return searchqueries

    def get_verbatims_by_features(self, top_features, topic, taggedvids):
        # get search queries
        searchqueries = self._create_feature_search_query(top_features, topic, taggedvids)

        # executing search queries
        vidcollection = []
        for sq in searchqueries:
            _type = sq[0]  # type of search query
            query = sq[1]  # search query
            dbcolmap = self.FEATURECOLLECTIONMAP[_type]  # db and collection mapping
            vids = [d['_id'] for d in self.client[dbcolmap['db']][dbcolmap['col']].find(query, no_cursor_timeout=True)]
            vidcollection.append(vids)


        # getting intersection
        intersecting_vids = []
        for i, vids in enumerate(vidcollection):
            if i == 0:
                intersecting_vids = vids
            else:
                intersecting_vids = list(set(intersecting_vids) & set(vids))

        # if not enough verbatims, add more from the lists
        if len(intersecting_vids) < self.MAXVERBATIMS:
            for vids in vidcollection:
                for vid in vids:
                    if len(intersecting_vids) < self.MAXVERBATIMS:
                        if vid not in intersecting_vids:
                            intersecting_vids.append(vid)
                    else:
                        break
        else:
            # otherwise forcefully take only MAXVERBATIMS ENTRIES
            intersecting_vids = intersecting_vids[:self.MAXVERBATIMS]


        return [d for d in self.client.indexes.verbatims.find({'_id': {'$in': intersecting_vids}}, no_cursor_timeout=True)]

    def predict(self, insight, topic):
        # getting tagged verbatim ids
        taggedvids = self.get_verbatim_ids(insight)

        # getting top features for insight
        top_features = [d['feature'] for d in self.client.modal.modals.find({'insightid': insight['_id']}).sort(
            [("feature.weight", DESCENDING)])[:50]]

        potentialverbatims = self.get_verbatims_by_features(top_features, topic, taggedvids)

        doc_similarities = []
        # iterating through all verbatims
        for d in potentialverbatims:
            similarity = 0.0
            featuretypes = self.extract_features([d['_id']])
            for features in featuretypes:
                for feature in features:
                    # check to see if feature exists in modal
                    id_feature = self.hash_feature(insight, feature)
                    item = self.client.modal.modals.find_one({'_id': id_feature})
                    if item and 'feature' in item and 'weight' in item['feature']:
                        similarity += item['feature']['weight']

            # append doc similarity to list
            doc_similarities.append((d['_id'], similarity))

        self.store_predictions(insight, topic, doc_similarities)
        # set compiled true for insight and topic
        self.store_settings(insight, topic, True)

    def store_settings(self, insight, topic, ifcompiled):
        settingid = self.hash_setting(insight, topic)
        setting = self.client.modal.settings.find_one({'_id': settingid})
        if not setting:
            self.client.modal.settings.insert({
                '_id': settingid,
                'insightid': insight['_id'],
                'project': insight['project'],
                'topic': topic,
                'compiled': ifcompiled,
                'iter': 0
            })
        else:
            self.client.modal.settings.update(
                {'_id': settingid},
                {'$set': {
                    'compiled': ifcompiled,
                    'iter': 0
                }}
            )

    def get_suggested(self, insight, topic, get='vids'):
        if get == 'vids':
            return [d for d in self.client.modal.predictions.find({'insightid': insight['_id'], 'topic': topic}).sort(
                [("score", DESCENDING)])]
        else:
            map_vid_scores = {}
            for d in self.client.modal.predictions.find({'insightid': insight['_id'], 'topic': topic}).sort(
                    [("score", DESCENDING)]):
                map_vid_scores[d['vid']] = d['score']

            docs = []
            for d in self.client.indexes.verbatims.find({'_id': {'$in': map_vid_scores.keys()}},
                                                        {'verbatim': 1, 'topic': 1}):
                d['score'] = map_vid_scores[d['_id']]
                docs.append(d)

            sorted_docs = sorted(docs, key=lambda d: d['score'], reverse=True)

            return sorted_docs
