# -*- coding: utf-8 -*-
from pymongo import MongoClient, DESCENDING, ASCENDING
from bson.code import Code
from bson.son import SON
import hashlib as hl
from datetime import datetime
import resources as rs
import sys, traceback

MAXASSOCIATION = 200


class InsightIndexer(object):
    def __init__(self, host=rs.HOST):
        self.db = MongoClient(host).indexes

    def _store_verbatim(self, _id, verbatim, topic):
        # store verbatim
        if not self.db.verbatims.find_one({'_id': _id}):
            self.db.verbatims.insert({'_id': _id, 'verbatim': verbatim, "topic": topic})

    def _store_options(self, _id, options):
        self.db.verbatims.update(
            {'_id': _id},
            {"$set": {
                "options": options
            }}
        )

    def _store_hashtags(self, _id, hashtags):
        self.db.verbatims.update(
            {'_id': _id},
            {"$set": {
                "hashtags": hashtags
            }}
        )

    def _store_mentions(self, _id, mentions):
        self.db.verbatims.update(
            {'_id': _id},
            {"$set": {
                "mentions": mentions
            }}
        )

    def _store_urls(self, _id, urls):

        # creating domain List
        domainList = []
        if 'domains' in urls:
            for domain in urls['domains']:
                if domain['_id'] not in domainList:
                    domainList.append(domain['_id'])

        self.db.verbatims.update(
            {'_id': _id},
            {"$set": {
                "urls": urls,
                "domainList": domainList,
            }}
        )

    def _store_emotions(self, _id, emotion):
        self.db.verbatims.update(
            {'_id': _id},
            {"$set": {
                "emotion": emotion
            }}
        )

    def _store_custom_data(self, _id, data):
        self.db.verbatims.update(
            {'_id': _id},
            {"$set": data}
        )

    def _store_entities(self, _id, entities, topic):
        self.db.verbatimners.update(
            {'_id': _id},
            {"$set": {
                "entities": entities,
                "topic": topic
            }}
            , upsert=True
        )

    def _store_keywords(self, _id, keywords, topic):
        keywords_to_store = {
            'ids': [],
            'keywords': [],
            'weights': []
        }
        for k in keywords:
            keyword = k[1]
            weight = k[0]

            _id_keyword = self._hash(keyword)
            # store keyword
            o = self.db.keywords.find_one({'_id': _id_keyword})
            if not o:
                self.db.keywords.insert({'_id': _id_keyword, 'keyword': keyword, 'topics': [topic]})
            else:
                if 'topics' in o:
                    if topic not in o['topics']:
                        self.db.keywords.update(
                            {'_id': _id_keyword},
                            {'$push': {'topics': topic}}
                        )
                else:
                    self.db.keywords.update(
                        {'_id': _id_keyword},
                        {'$set': {'topics': [topic]}}
                    )
            #
            keywords_to_store['ids'].append(_id_keyword)
            keywords_to_store['keywords'].append(keyword)
            keywords_to_store['weights'].append(weight)

        # store verbatim keyword relation
        if not self.db.verbatimkeywords.find_one({'_id': _id}):
            self.db.verbatimkeywords.insert(
                {'_id': _id, '_id_keywords': keywords_to_store['ids'], 'weight': keywords_to_store['weights'],
                 'keywords': keywords_to_store['keywords'], "topic": topic})
        else:
            self.db.verbatimkeywords.update(
                {'_id': _id},
                {'$set':
                    {
                        '_id_keywords': keywords_to_store['ids'], 'weight': keywords_to_store['weights'],
                        'keywords': keywords_to_store['keywords'], "topic": topic
                    }
                }
            )

    def _store_concepts(self, _id, concepts, topic):
        # TODO add field topics
        concepts_to_store = {
            'ids': [],
            'concepts': [],
            'weight': [],
            'relation': []
        }

        for keyword in concepts:
            if 'concepts' in concepts[keyword]:
                if concepts[keyword]['concepts']:
                    for concept in concepts[keyword]['concepts']:
                        edge = concepts[keyword]['concepts'][concept]
                        _id_concept = self._hash(concept)

                        # store concept
                        if not self.db.concepts.find_one({'_id': _id_concept}):
                            self.db.concepts.insert({'_id': _id_concept, 'concept': concept})

                        #
                        if _id_concept not in concepts_to_store['ids']:
                            concepts_to_store['ids'].append(_id_concept)
                            concepts_to_store['concepts'].append(edge['end'])
                            concepts_to_store['weight'].append(edge['weight'])
                            concepts_to_store['relation'].append(edge['rel'])

        # store relation
        if not self.db.verbatimconcepts.find_one({'_id': _id}):
            self.db.verbatimconcepts.insert(
                {'_id': _id, '_id_concepts': concepts_to_store['ids'], 'concepts': concepts_to_store['concepts'],
                 'relations': concepts_to_store['relation'], 'weights': concepts_to_store['weight'], "topic": topic}
            )
        else:
            self.db.verbatimconcepts.update(
                {'_id': _id,},
                {'$set':
                    {
                        '_id_concepts': concepts_to_store['ids'], 'concepts': concepts_to_store['concepts'],
                        'relations': concepts_to_store['relation'], 'weights': concepts_to_store['weight'],
                        "topic": topic
                    }
                }
            )

    def _store_pos(self, _id, pos, topic):
        for p in pos:
            listwords = []
            listweightwords = []
            listidwords = []
            items = pos[p]
            for item in items:
                idWord = self._hash(item['term'])

                # adding word in pos collectio n
                o = self.db[p + "s"].find_one({"_id": idWord})
                if not o:
                    self.db[p + "s"].insert({"_id": idWord, "word": item['term'], 'topics': [topic]})
                else:
                    if 'topics' in o:
                        if topic not in o['topics']:
                            self.db[p + "s"].update(
                                {"_id": idWord},
                                {'$push': {'topics': topic}}
                            )
                    else:
                        self.db[p + "s"].update(
                            {"_id": idWord},
                            {'$set': {'topics': [topic]}}
                        )

                # is item already exists
                if idWord not in listidwords:
                    listidwords.append(idWord)
                    listwords.append(item['term'])
                    listweightwords.append(item['count'])
                else:
                    index = listidwords.index(idWord)
                    listweightwords[index] += item['count']

            self.db["verbatim" + p + "s"].update({"_id": _id}, {"$set": {
                "topic": topic,
                "_id_words": listidwords,
                "words": listwords,
                "weights": listweightwords,
            }}, upsert=True)

    def _store_terms(self, _id, terms, topic):
        weights = []
        _id_terms = []
        termlist = []
        for term in terms:
            _id_term = self._hash(term)

            # store terms
            o = self.db.terms.find_one({'_id': _id_term})
            if not o:
                self.db.terms.insert({'_id': _id_term, 'term': term, 'topics': [topic]})
            else:
                if topic not in o['topics']:
                    self.db.terms.update(
                        {'_id': _id_term},
                        {'$push': {'topics': topic}}
                    )

            weights.append(terms[term])
            _id_terms.append(_id_term)
            termlist.append(term)

        # store verbatim term relation
        o = self.db.verbatimterms.find_one({'_id': _id})
        if not o:
            self.db.verbatimterms.insert({
                '_id': _id, '_id_terms': _id_terms, 'terms': termlist, "topic": topic
            })
        else:
            _id_terms_new = []
            _terms_new = []
            for term in terms:
                if term not in o['terms']:
                    _id_term = self._hash(term)
                    _id_terms_new.append(_id_term)
                    _terms_new.append(term)

            self.db.verbatimterms.update(
                {'_id': _id},
                {'$push': {
                    '_id_terms': {'$each': _id_terms_new},
                    'terms': {'$each': _terms_new}
                }
                })

    def _hash(self, _str):
        try:
            _id = hl.md5(_str.encode('utf8')).hexdigest()
            return _id
        except:
            try:
                _id = hl.md5(_str).hexdigest()
                return _id
            except:
                e = sys.exc_info()[0]
                print ("ERROR %s", e)
                traceback.print_exc(file=sys.stdout)
            e = sys.exc_info()[0]
            print ("ERROR %s", e)
            traceback.print_exc(file=sys.stdout)

    def _set_default_settings(self, topic, lang, project, user, company):
        topicid = hl.md5(topic + project).hexdigest()
        if not self.db.settings.find_one({"_id": topicid}):
            self.db.settings.insert({
                "_id": topicid,
                "topic": topic,
                "lang": lang,
                "dir": "unnamed",
                "users": [user],
                "project": project,
                "company": company,
            })

    def index(self, text, text_raw, topic, project, user, company, keywords, concepts, pos, terms, sentiment, hashtags, mentions, urls, emotion,
              entities, lang, prefetcheddata=None):
        # unique id for document
        _id = hl.md5(text_raw + topic).hexdigest()
        self._store_verbatim(_id, text_raw, topic)
        if keywords:
            self._store_keywords(_id, keywords, topic)
        if concepts:
            self._store_concepts(_id, concepts, topic)
        if pos:
            self._store_pos(_id, pos, topic)

        if terms:
            self._store_terms(_id, terms, topic)

        if emotion:
            self._store_emotions(_id, emotion)

        if entities:
            self._store_entities(_id, entities, topic)

        if sentiment and sentiment['polarity']:
            self._store_options(_id, sentiment)

        if hashtags:
            self._store_hashtags(_id, hashtags)

        if mentions:
            self._store_mentions(_id, mentions)

        if urls:
            self._store_urls(_id, urls)

        if prefetcheddata:
            self._store_custom_data(_id, prefetcheddata)

        # finally
        self._set_default_settings(topic, lang, project, user, company)


class WordCloudIndexer(object):
    ITERATION = 1

    def __init__(self, host=rs.HOST):
        self._client = MongoClient(host)

    def _create_global_word_frequency_collection(self, topic, colname, fieldname, outdbname):
        print colname, fieldname
        mapper = Code(
            """
                    function(){
                        this.""" + fieldname + """.forEach(function(item){
                        emit(item, 1)
                    })
                }
                """
        )

        reducer = Code(
            """
                    function(key, counts){
                        return Array.sum(counts)
                    }
                """
        )

        # map reduce to compute word and frequency
        result = self._client.indexes[colname].map_reduce(
            mapper,
            reducer,
            query={"topic": topic},
            out=SON([("replace", topic), ("db", outdbname)]),
            full_response=True)

        # create indexe on value
        self._client[outdbname][topic].create_index([("value", DESCENDING)])
        print result

    def _compute_associations(self, topic, colname, fieldname, dbname, lang):
        for word in self._client[dbname][topic].find().sort([("value", DESCENDING)]):
            _map = {}

            # get association
            for item in self._client.indexes[colname].find(
                    {"topic": topic, fieldname: {"$in": [word['_id']]}},
                    {"_id": -1, fieldname: 1},
                    no_cursor_timeout=True
            ):

                for w in item[fieldname]:
                    ifdelete = False
                    if fieldname == "keywords":
                        ifdelete = self.if_delete_keyword(w, lang)
                    elif fieldname == "terms":
                        ifdelete = self.if_delete_term(w, lang)
                    elif fieldname == "concepts":
                        ifdelete = self.if_delete_concept(w, lang)

                    if not ifdelete:
                        if w not in _map:
                            _map[w] = 1
                        else:
                            _map[w] += 1

            # map to list
            _list = [(w, _map[w]) for w in _map]
            del _map  # free memory
            _list = sorted(_list, key=lambda t: t[1], reverse=True)  # sort list

            count = len(_list)

            if count > MAXASSOCIATION:
                p = float(MAXASSOCIATION) / count
            else:
                p = 1
            limit = int(count * p)

            # savable format
            top_freq_associates = [w[0] for w in _list[0:limit]]

            # save associations
            self._client[dbname][topic].update(
                {"_id": word['_id']},
                {"$set": {
                    "associates": top_freq_associates
                }}
            )

            if self.ITERATION % 10000 == 0:
                print "treaded", self.ITERATION, "words"
            self.ITERATION += 1

        # finally create index
        self._client[dbname][topic].create_index([("associates", ASCENDING)])

    @staticmethod
    def if_delete_keyword(k, lang):
        ifdelete = False
        # remove keywords of length less than 3
        if len(k) < 3:
            # delete
            ifdelete = True

        # remove stop words
        if not ifdelete:
            if k.encode('utf8') in rs.stop_words[lang]:
                ifdelete = True

        return ifdelete

    @staticmethod
    def if_delete_term(k, lang):
        ifdelete = False
        # remove keywords of length less than 3
        if len(k) < 3:
            # delete
            ifdelete = True

        # remove stop words
        if not ifdelete:
            if k.encode('utf8') in rs.stop_words[lang]:
                ifdelete = True

        return ifdelete

    @staticmethod
    def if_delete_concept(c, lang):
        ifdelete = False
        # remove concepts of length less than 3
        if len(c[3]) < 3:
            # delete
            ifdelete = True

        # remove stop words
        if not ifdelete:
            if c[3].encode('utf8') in rs.stop_words[lang]:
                ifdelete = True

        return ifdelete

    def _clean_collection(self, topic, colname, fieldname, dbname, lang):
        if fieldname == "keywords":
            # cleaning keywords
            for d in self._client.wckeywords[topic].find(no_cursor_timeout=True):
                k = d['_id']
                if self.if_delete_keyword(k, lang):
                    self._client.wckeywords[topic].remove({"_id": d['_id']})

        elif fieldname == "terms":
            # cleaning terms
            for d in self._client.wcterms[topic].find(no_cursor_timeout=True):
                k = d['_id']
                if self.if_delete_term(k, lang):
                    self._client.wcterms[topic].remove({"_id": d['_id']})
        elif fieldname == "concepts":
            for d in self._client.wcconcepts[topic].find(no_cursor_timeout=True):
                c = d['_id'].split('/')
                if self.if_delete_concept(c, lang):
                    self._client.wcconcepts[topic].remove({"_id": d['_id']})
                    # print "Skipping cleaning concepts"

    def _clean_index_collection(self, col, topic, fieldname, lang):
        if fieldname == "keywords":
            for d in self._client.indexes.verbatimkeywords.find(no_cursor_timeout=True):
                idks = []
                ks = []
                wks = []
                for idk, k, wk in zip(d['_id_keywords'], d['keywords'], d['weight']):
                    if not self.if_delete_keyword(k, lang):
                        idks.append(idk)
                        ks.append(k)
                        wks.append(wk)

                self._client.indexes.verbatimkeywords.update(
                    {"_id": d['_id']},
                    {"$set": {
                        '_id_keywords': idks,
                        'keywords': ks,
                        'weight': wks
                    }}
                )
            # print "Skipping cleaning keywords"

        elif fieldname == "concepts":
            for d in self._client.indexes.verbatimconcepts.find(no_cursor_timeout=True):
                idcs = []
                cs = []
                wcs = []
                for idc, c, wc in zip(d['_id_concepts'], d['concepts'], d['weights']):
                    if not self.if_delete_concept(c.split('/'), lang):
                        idcs.append(idc)
                        cs.append(c)
                        wcs.append(wc)

                self._client.indexes.verbatimconcepts.update(
                    {"_id": d['_id']},
                    {"$set": {
                        '_id_concepts': idcs,
                        'concepts': cs,
                        'weights': wcs
                    }}
                )

    def index(self, topic):
        print "compute wordcloud data on", topic
        for col in rs.COLLECTION_ASSOCIATES_NAME_LIST:
            print "col", col, "---"
            lang = None
            o = self._client.indexes.settings.find_one({"topic": topic})
            if o and 'lang' in o:
                lang = o['lang']
            else:
                print "Dataset language not defined!", topic
                exit(2)

            print "cleaning index collection"
            self._clean_index_collection(
                topic,
                col,
                rs.COLLECTION_ASSOCIATES_FIELD_MAP[col]['words'],
                lang
            )

            print "populating word frequency collection"
            self._create_global_word_frequency_collection(
                topic,
                col,
                rs.COLLECTION_ASSOCIATES_FIELD_MAP[col]['words'],
                rs.COLLECTION_ASSOCIATES_FIELD_MAP[col]['dbname']
            )

            print "cleaning collection"
            self._clean_collection(
                topic,
                col,
                rs.COLLECTION_ASSOCIATES_FIELD_MAP[col]['words'],
                rs.COLLECTION_ASSOCIATES_FIELD_MAP[col]['dbname'],
                lang
            )

            # DEPRECATED
            # print "computing word associations"
            # self._compute_associations(
            #     topic,
            #     col,
            #     rs.COLLECTION_ASSOCIATES_FIELD_MAP[col]['words'],
            #     rs.COLLECTION_ASSOCIATES_FIELD_MAP[col]['dbname'],
            #     lang
            # )
