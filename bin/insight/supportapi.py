# -*- coding: utf-8 -*-
import sys, traceback
from urllib3 import HTTPConnectionPool
import json
import resources as rs


class SupportAPI(object):
    def __init__(self):
        self.pool = HTTPConnectionPool(rs.SUPPORTAPI_HOST)

    def get_ner(self, text):
        r = self.pool.request('POST', '/ner/', fields={"text": text})
        data = json.loads(r.data)

        # sort entities and their frequencies
        freq = {}
        ent = {}
        if 'entities' in data:
            for e in data['entities']:
                if e[1] != u'O':
                    e[1] = e[1].replace('.', '')
                    if e[1] not in ent:
                        ent[e[1]] = [e[0]]
                        freq[e[1]] = {}
                        freq[e[1]][e[0]] = 1
                    else:
                        if e[0] not in ent[e[1]]:
                            ent[e[1]].append(e[0])
                            freq[e[1]][e[0]] = 1
                        else:
                            freq[e[1]][e[0]] += 1

        # format entities
        entities = {}
        for t in ent:
            if t not in entities:
                entities[t] = {}
            for e in ent[t]:
                if e not in entities[t]:
                    entities[t][e] = freq[t][e]

        return entities

    def get_sentiment(self, text):
        r = self.pool.request('POST', '/sentiment/', fields={"text": text})
        data = json.loads(r.data)
        return data
