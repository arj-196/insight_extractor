# -*- coding: utf-8 -*-
import re
from urlparse import urlparse
import sys, traceback


class HashtagTagger(object):
    ptnlang = {
        'en': "#\w+",
        'fr': "#[\wùÙûÛüÜÿŸàÀâÂæÆçÇéÉèÈêÊëËïÏîÎôÔœŒ]+"
    }
    ptn = None

    def __init__(self, lang):
        self.ptn = re.compile(self.ptnlang[lang])

    def tag(self, text):
        _map = {}
        r = re.findall(self.ptn, text)
        for w in r:
            w = w.lower()
            if w not in _map:
                _map[w] = 1
            else:
                _map[w] += 1
        return _map


class MentionTagger(object):
    ptnlang = {
        'en': "@\w+",
        'fr': "@[\wùÙûÛüÜÿŸàÀâÂæÆçÇéÉèÈêÊëËïÏîÎôÔœŒ]+"
    }
    ptn = None

    def __init__(self, lang):
        self.ptn = re.compile(self.ptnlang[lang])

    def tag(self, text):
        _map = {}
        r = re.findall(self.ptn, text)
        for w in r:
            w = w.lower()
            if w not in _map:
                _map[w] = 1
            else:
                _map[w] += 1
        return _map


class URLTagger(object):
    p = "((?:[a-z][\w-]+:(?:/{1,3}|[a-z0-9%])|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))"
    ptn = None

    def __init__(self, lang):
        self.ptn = re.compile(self.p)

    def tag(self, text):
        _domains = {}
        _urls = []

        r = re.findall(self.ptn, text)
        for w in r:
            w = w[0]
            try:
                parsed_uri = urlparse(w)
                if parsed_uri.hostname:
                    if parsed_uri.hostname not in _domains:
                        _domains[parsed_uri.hostname] = 1
                    else:
                        _domains[parsed_uri.hostname] += 1

                if w not in _urls:
                    try:
                        _urls.append(w.encode('utf8'))
                    except:
                        pass
            except:
                pass
        domain_list = []
        for d in _domains:
            domain_list.append({
                '_id': d,
                'value': _domains[d]
            })

        return {
            'domains': domain_list,
            "urls": _urls
        }

    def get_domain(self, url):
        # TODO complete
        return url
