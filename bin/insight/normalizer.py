# -*- coding: utf-8 -*-
import resources as rs
import re


class SimpleNormalizer(object):
    def __init__(self, lang):
        self.lang = lang
        self.stop_words = rs.stop_words[lang]

    def normalize_text(self, text, ifdecode=True):
        s = text.lower().strip()
        # remove all puntutations
        for p in rs.puntuations:
            try:
                if ifdecode:
                    s = s.replace(p.decode('utf8'), " ")
                else:
                    s = s.replace(p, " ")
            except UnicodeDecodeError:
                s = s.replace(p, " ")

        # remove multiple spaces
        s = re.sub('\n', '', s)
        s = re.sub('\t', '', s)
        s = re.sub('\d', '', s)
        s = re.sub(' +', ' ', s)
        return s

    def normalize_word(self, text):
        s = text.lower().strip()
        # remove multiple spaces
        s = re.sub('\n', '', s)
        s = re.sub('\t', '', s)
        s = re.sub('\d', '', s)
        s = re.sub(' +', ' ', s)
        return s


    def clean_tokens(self, tokens):
        stop_words = None
        if self.lang == 'en':
            stop_words = rs.stop_words['en']
        elif self.lang == 'fr':
            stop_words = rs.stop_words['fr']

        if stop_words:
            for stop_word in stop_words:
                stop_word = stop_word.decode('utf8')
                if stop_word in tokens:
                    tokens = filter(lambda a: a != stop_word, tokens)
            return tokens
        else:
            raise Exception("Cannot handle language", self.lang)

    def clean_keywords(self, keywords):
        filteredlist = []
        stop_words = None
        if self.lang == 'en':
            stop_words = rs.stop_words['en']
        elif self.lang == 'fr':
            stop_words = rs.stop_words['fr']

        if stop_words:
            for t in keywords:
                status = True
                for st in stop_words:
                    if t[1].encode('utf8') == st:
                        status = False
                        break

                if status:
                    filteredlist.append(t)
            return filteredlist
        else:
            raise Exception("Cannot handle language", self.lang)