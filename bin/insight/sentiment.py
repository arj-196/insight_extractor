import resources as rs
import supportapi
from pattern.text.en import sentiment as sentimentEN
from pattern.text.fr import sentiment as sentimentFR


class SentimentTagger(object):
    def __init__(self, lang):
        self.lang = lang
        self.api = supportapi.SupportAPI()

    def tag(self, text):
        if self.lang == 'fr':
            # return self.api.get_sentiment(text)
            s = sentimentFR(text)
            return {
                "polarity": s[0],
                "subjectivity": s[1],
            }
        else:
            s = sentimentEN(text)
            return {
                "polarity": s[0],
                "subjectivity": s[1],
            }
