# -*- coding: utf-8 -*-
# HOST = "localhost:27017"
# HOST = "localhost:27018"
HOST = "mongodb://151.80.41.13:27018"
SUPPORTAPI_HOST = "151.80.41.13:5000"
# SUPPORTAPI_HOST = "localhost:5000"

CONCEPTNET_RELATION_MAPPING = {
    # '/r/CapableOf': {'threshold': 1.0, 'weight': 1},
    '/r/AtLocation': {'threshold': 1.0, 'weight': 1},
    '/r/CreatedBy': {'threshold': 1.0, 'weight': 1},
    '/r/DefinedAs': {'threshold': 1.0, 'weight': 3},
    '/r/DerivedFrom': {'threshold': 1.0, 'weight': 1},
    '/r/HasA': {'threshold': 1.0, 'weight': 2},
    '/r/HasContext': {'threshold': 1.0, 'weight': 1},
    '/r/HasFirstSubevent': {'threshold': 1.0, 'weight': 1},
    '/r/HasPrerequiste': {'threshold': 1.0, 'weight': 1},
    '/r/HasProperty': {'threshold': 1.0, 'weight': 0.5},
    '/r/HasSubevent': {'threshold': 1.0, 'weight': 0.5},
    '/r/InstanceOf': {'threshold': 1.0, 'weight': 2},
    '/r/IsA': {'threshold': 1.0, 'weight': 2},
    '/r/MadeOf': {'threshold': 1.0, 'weight': 2},
    '/r/MemberOf': {'threshold': 1.0, 'weight': 2},
    '/r/Mot': {'threshold': 1.0, 'weight': 1},
    '/r/MotivatedByGoal': {'threshold': 1.0, 'weight': 1},
    '/r/PartOf': {'threshold': 1.0, 'weight': 2},
    '/r/ReceivesAction': {'threshold': 1.0, 'weight': 1},
    '/r/RelatedTo': {'threshold': 1.0, 'weight': 1},
    '/r/SimilarTo': {'threshold': 1.0, 'weight': 2},
    '/r/Synonym': {'threshold': 1.0, 'weight': 3},
    '/r/TranslationOf': {'threshold': 1.0, 'weight': 8},
    '/r/UsedFor': {'threshold': 1.0, 'weight': 1},
    '/r/wordnet/adjectivePertainsTo': {'threshold': 1.0, 'weight': 1},
    '/r/wordnet/adverbPertainsTo': {'threshold': 1.0, 'weight': 1},
}

# CONCEPTNET_RELATION_MAPPING = {
#     '/r/CapableOf': {'threshold': 2.5, 'weight': 1},
#     '/r/AtLocation': {'threshold': 2.5, 'weight': 1},
#     '/r/CreatedBy': {'threshold': 1.5, 'weight': 1},
#     '/r/DefinedAs': {'threshold': 1.1, 'weight': 3},
#     '/r/DerivedFrom': {'threshold': 1.5, 'weight': 1},
#     '/r/HasA': {'threshold': 2.0, 'weight': 2},
#     '/r/HasContext': {'threshold': 1.0, 'weight': 1},
#     '/r/HasFirstSubevent': {'threshold': 2.0, 'weight': 1},
#     '/r/HasPrerequiste': {'threshold': 2.5, 'weight': 1},
#     '/r/HasProperty': {'threshold': 2.0, 'weight': 0.5},
#     '/r/HasSubevent': {'threshold': 2.0, 'weight': 0.5},
#     '/r/InstanceOf': {'threshold': 1.0, 'weight': 2},
#     '/r/IsA': {'threshold': 1.5, 'weight': 2},
#     '/r/MadeOf': {'threshold': 1.5, 'weight': 2},
#     '/r/MemberOf': {'threshold': 1.5, 'weight': 2},
#     '/r/Mot': {'threshold': 2.0, 'weight': 1},
#     '/r/MotivatedByGoal': {'threshold': 2.0, 'weight': 1},
#     '/r/PartOf': {'threshold': 2.0, 'weight': 2},
#     '/r/ReceivesAction': {'threshold': 2.0, 'weight': 1},
#     '/r/RelatedTo': {'threshold': 2.0, 'weight': 1},
#     '/r/SimilarTo': {'threshold': 2.5, 'weight': 2},
#     '/r/Synonym': {'threshold': 1.0, 'weight': 3},
#     '/r/TranslationOf': {'threshold': 1.0, 'weight': 8},
#     '/r/UsedFor': {'threshold': 2.5, 'weight': 1},
#     '/r/wordnet/adjectivePertainsTo': {'threshold': 1.0, 'weight': 1},
#     '/r/wordnet/adverbPertainsTo': {'threshold': 1.0, 'weight': 1},
# }

CONCEPTNET_RELATION_BAN_MAPPING = [
    '/r/CapableOf',
    '/r/Antonym',
    '/r/Causes',
    '/r/CausesDesire',
    '/r/CompoundDerivedFrom',
    '/r/DesireOf',
    '/r/Desires',
    '/r/Entails',
    '/r/EtymologicallyDerivedFrom',
    '/r/NotCapableOf',
    '/r/NotDesires',
    '/r/NotHasA',
    '/r/NotHasProperty',
    '/r/NotIsA',
    '/r/Attribute',
    '/r/dpedia/genre ',
]

EMOTIONS = {
    'en': {

        'Open': [

            'understanding', 'confident', 'reliable', 'easy', 'amazed', 'free', 'sympathetic', 'interested',
            'satisfied', 'receptive', 'accepting', 'kind',
        ],

        'Happy': [

            'great', 'gay', 'joyous', 'lucky', 'fortunate', 'delighted', 'overjoyed', 'gleeful', 'thankful',
            'important', 'festive', 'ecstatic', 'satisfied', 'glad', 'cheerful', 'sunny', 'merry', 'elated', 'jubilant',
        ],

        'Alive': [

            'playful', 'courageous', 'energetic', 'liberated', 'optimistic', 'provocative', 'impulsive', 'free',
            'frisky', 'animated', 'spirited', 'thrilled', 'wonderful',
        ],

        'Love': [

            'loving', 'considerate', 'affectionate', 'sensitive', 'tender', 'devoted', 'attracted', 'passionate',
            'admiration', 'warm', 'touched', 'sympathy', 'close', 'loved', 'comforted', 'drawn toward',
        ],

        'Interested': [

            'concerned', 'affected', 'fascinated', 'intrigued', 'absorbed', 'inquisitive', 'nosy', 'snoopy',
            'engrossed', 'curious',
        ],

        'Positive': [

            'eager', 'keen', 'earnest', 'intent', 'anxious', 'inspired', 'determined', 'excited', 'enthusiastic',
            'bold', 'brave', 'daring', 'challenged', 'optimistic', 're-enforced', 'confident', 'hopeful',
        ],

        'Angry': [

            'irritated', 'enraged', 'hostile', 'insulting', 'sore', 'annoyed', 'upset', 'hateful', 'unpleasant',
            'offensive', 'bitter', 'aggressive', 'resentful', 'inflamed', 'provoked', 'incensed', 'infuriated', 'cross',
            'worked up', 'boiling', 'fuming', 'indignant',
        ],

        'Depressed': [

            'lousy', 'disappointed', 'discouraged', 'ashamed', 'powerless', 'diminished', 'guilty', 'dissatisfied',
            'miserable', 'detestable', 'repugnant', 'despicable', 'disgusting', 'abominable', 'terrible', 'in despair',
            'sulky', 'bad', 'a sense of loss',
        ],

        'Confused': [

            'upset', 'doubtful', 'uncertain', 'indecisive', 'perplexed', 'embarrassed', 'hesitant', 'shy', 'stupefied',
            'disillusioned', 'unbelieving', 'skeptical', 'distrustful', 'misgiving', 'lost', 'unsure', 'uneasy',
            'pessimistic', 'tense',
        ],

        'Helpless': [

            'incapable', 'alone', 'paralyzed', 'fatigued', 'useless', 'inferior', 'vulnerable', 'empty', 'forced',
            'hesitant', 'despair', 'frustrated', 'distressed', 'woeful', 'pathetic', 'tragic', 'in a stew', 'dominated',
        ],

        'Indifferent': [

            'insensitive', 'dull', 'nonchalant', 'neutral', 'reserved', 'weary', 'bored', 'preoccupied', 'cold',
            'disinterested', 'lifeless',
        ],

        'Afraid': [

            'fearful', 'terrified', 'suspicious', 'anxious', 'alarmed', 'panic', 'nervous', 'scared', 'worried',
            'frightened', 'timid', 'shaky', 'restless', 'doubtful', 'threatened', 'cowardly', 'quaking', 'menaced',
            'wary',
        ],

        'Hurt': [

            'crushed', 'tormented', 'deprived', 'pained', 'tortured', 'dejected', 'rejected', 'injured', 'offended',
            'afflicted', 'aching', 'victimized', 'heartbroken', 'agonized', 'appalled', 'humiliated', 'wronged',
            'alienated',
        ],

        'Sad': [

            'tearful', 'sorrowful', 'pained', 'grief', 'anguish', 'desolate', 'desperate', 'pessimistic', 'unhappy',
            'lonely', 'grieved', 'mournful', 'dismayed',
        ],

    },
    'fr': {

        'Tranquillité':

            [

                "tranquillité", "à l'aise", "absorbé", "adouci", "affermi", "aimant", "alerte", "allégé", "amadoué",
                "apaisé", "apitoyé", "assouvi", "attendri", "attentif", "bien disposé", "calme", "centré", "comblé",
                "compatissant", "compréhensif", "concentré", "concerné", "conciliant", "confiant", "confortable",
                "conforté", "consolé", "consolidé", "cool", "d'humeur amicale", "d'humeur insouciante",
                "dans l'indicible", "dans l'unité", "dans la plénitude", "déchargé", "décontracté", "décrispé",
                "délassé", "délesté", "délivré", "détaché", "détendu", "déterminé", "disponible", "dispos", "dubitatif",
                "égal", "empli de bienveillance", "empli de tendresse", "empressé", "en confiance", "en harmonie avec",
                "en paix", "encouragé", "épanoui", "équilibré", "éveillé", "flegmatique", "fortifié", "gavé",
                "gonflé à bloc", "impassible", "impavide", "imperturbable", "impliqué", "incrédule", "inébranlable",
                "insouciant", "inspiré", "intéressé", "léger", "libéré", "libre", "miséricordieux", "mobilisé",
                "nonchalant", "nourri", "paisible", "peinard", "placide", "plein d'affection",
                "plein d'amour", "plein d'application", "plein d'aplomb", "plein d'appréciation", "plein d'assurance",
                "plein d'empathie", "plein d'énergie", "plein d'équanimité", "plein d'inclination",
                "plein de bénignité", "plein de chaleur", "plein de commisération", "plein de compassion",
                "plein de douceur", "plein de gratitude", "plein de mansuétude", "plein d'opiniâtreté",
                "plein de persévérance", "plein de pitié", "plein de prévenance", "plein de quiétude",
                "plein de révérence", "plein de sympathie", "plein de tendresse", "pondéré", "posé", "présent",
                "proche", "quiet", "radouci", "raffermi", "rafraîchi", "ragaillardi", "rassasié", "rasséréné",
                "rassuré", "réchauffer le coeur", "réconforté", "reconnaissant", "récréé", "régénéré", "regonflé",
                "relâché", "relax", "relaxé", "relié", "remonté", "renforcé", "reprendre", "haleine", "repu",
                "renforcé", "résolu", "rester de marbre", "revigoré", "satisfait", "sceptique", "sécurisé",
                "sensibilisé", "sensible", "serein", "soulagé", "stable stimulé", "stoïque", "sûr de soi",
                "tenir au coeur", "tranquille", "vacant", "zen",
            ]
        ,

        'Surprise':
            [

                "surprise", "abasourdi", "agité", "ahuri", "assommé", "avoir le souffle coupé", "blindé", "bluffé",
                "chancelant", "choqué", "confondu", "confus", "débordé", "déboussolé", "déconcerté", "décontenancé",
                "dépaysé", "dérangé", "dérouté", "désarçonné", "désemparé", "désorienté", "déstabilisé", "distrait",
                "ébahi", "éberlué", "effaré", "égaré", "embarrassé", "embrouillé", "emmêlé", "émotionné", "ému",
                "en boucher un coin", "en plein désarroi", "en rester comme deux ronds de flan", "en suspens",
                "engourdi", "épaté", "estomaqué", "étonné", "étourdi", "figé", "foudroyé", "frappé",
                "frappé de stupeur", "hébété", "hésitant", "impressionné", "incommodé", "indécis", "interdit",
                "interloqué", "interpellé", "intrigué", "irrésolu", "médusé", "ne pas savoir sur quel pied danser",
                "pantois", "perplexe", "pétrifié", "plombé", "pris au dépourvu", "pris de court", "prostré", "renversé",
                "rester bouche bée", "retourné", "saisi", "saturé", "scotché", "secoué", "sidéré", "soufflé",
                "stupéfait", "suffoqué", "surpris", "titubant", "tomber des nues", "touché", "troublé", "vacillant",
                "vide",

            ]
        ,

        'Joie':

            [

                "joie", "à la noce", "admiratif", "allègre", "aller au coeur", "altier", "amoureux", "amusé", "animé",
                "ardent", "attiré", "aux anges", "au septième ciel", "béat", "bienheureux", "bouillonnant",
                "bouleversé", "captivé", "charmé", "comblé", "content", "curieux", "d'humeur allègre",
                "d'humeur aventureuse", "d'humeur câline", "d'humeur badine", "d'humeur enjouée", "d'humeur espiègle",
                "d'humeur expansive", "d'humeur exubérante", "d'humeur folâtre", "d'humeur guillerette",
                "d'humeur jubilatoire", "d'humeur pétillante", "d'humeur rieuse", "d'humeur voluptueuse",
                "dans l'indicible", "dans l'unité", "dans la béatitude", "de bonne humeur", "déridé", "diverti",
                "ébloui", "égayé", "électrisé", "emballé", "embrasé", "émerveillé", "émoustillé", "empressé", "ému",
                "en effervescence", "en expansion", "en extase", "en liesse", "en pleine", "forme", "enchanté",
                "énergétisé", "enflammé", "engoué", "enivré", "enjoué", "enlevé", "ensorcelé", "enthousiaste",
                "envoûté", "épanoui", "épaté", "éperdu", "éperonné", "épris", "euphorique", "exalté", "excité",
                "extatique", "exultant", "fasciné", "fier", "fou de joie", "frémissant", "frétillant", "gai",
                "galvanisé", "grisé", "guilleret", "heureux", "hilare", "intrigué", "joyeux", "jubilant", "nourri",
                "optimiste", "passionné", "pétilant", "pétulant", "plein d'acharnement", "plein d'amour",
                "plein d'ardeur", "plein d'effervescence", "plein d'élan", "plein d'émoi", "plein d'entrain",
                "plein d'espoir", "plein d'hardiesse", "plein de courage", "plein d'énergie", "plein de félicité",
                "plein de ferveur", "plein de feu", "plein de fièvre", "plein de fougue", "plein de gratitude",
                "plein de hâte", "plein de liesse", "plein de pêtulance", "plein de ténacité", "plein de vénération",
                "plein de vertige", "plein de vie", "plein de vivacité", "plein de zète", "prendre feu", "radieux",
                "ravi", "ravigoté", "rayonnant", "réjoui", "remonté", "rempli de bonheur", "riant", "satisfait",
                "se fendre la gueule", "séduit", "stimulé", "submergé de joie", "surexcité", "survolté", "tenté",
                "titillé", "tonifié", "touché", "transi", "transporté", "troublé", "vibrant", "vif", "vivant",
                "vivifié",

            ]
        ,

        'Tristesse':

            [

                "tristesse", "à bout", "abattu", "accablé", "affecté", "affligé", "anesthésié", "assommé", "atteint",
                "atterré", "attristé", "avoir de la peine", "avoir du vague à l'âme", "avoir le bourdon",
                "avoir le cafard", "avoir le coeur brisé", "avoir le cœur gros", "avoir le cœur lourd",
                "avoir le cœur percé", "avoir le coeur serré", "avoir quelque chose sur le coeur", "baisser les bras",
                "battre sa coulpe", "bouleversé", "broyer du noir", "cafardeux", "chagriné", "chaviré",
                "comme une âme en peine", "confondu", "consterné", "contrarié", "contristé", "contrit", "coupable",
                "d'humeur chagrine", "d'humeur noire", "dans tous ses états", "de mauvaise humeur", "déchiré",
                "déconfit", "découragé", "déçu", "défait", "dégrisé", "démoralisé", "démotivé", "dépité", "déprimé",
                "désabusé", "désappointé", "désespéré", "désenchanté", "désillusionné", "désolé", "ébranlé", "embêté",
                "ému", "en avoir gros sur le coeur", "en avoir sec", "en désarroi", "en détresse", "en manque",
                "ennuyé", "éploré", "éteint", "étiolé", "étourdi", "fendre le coeur", "frappé honteux", "inconfortable",
                "insatisfait", "lourd", "lugubre", "mal à l'aise", "malheureux", "maussade", "mécontent",
                "mélancolique", "mi-figue", "mi-raisin", "misérable", "morne", "morose", "mortifié", "navré",
                "ne pas en mener large", "neurasthénique", "nostalgique", "paumé", "peiné", "penaud", "perturbé",
                "pessimiste", "plein de regret", "plombé", "plus mort que vif", "préoccupé", "prostré",
                "réduit au désespoir", "rembruni", "repentant", "résigné", "saisi", "sans élan", "sans entrain",
                "secoué", "sombre", "soucieux", "submergé", "surmené", "terrassé", "tourmenté", "tracassé",
                "traumatisé", "triste", "troublé", "vaseux",

            ]
        ,

        'Dégoût':
            [

                "dégoût", "avoir le coeur au bord des lèvres", "avoir le coeur dans la gorge", "dégoûté", "écoeuré",
                "plein de répugnance", "rebuté", "rempli de répulsion", "révulsé", "soulever le coeur",

            ]
        ,

        'Colère':

            [

                "colère", "à bout", "à cran", "à crin", "acariâtre", "acerbe", "acrimonieux", "agacé", "agité", "amer",
                "asticoté", "atrabilaire", "avoir les nerfs à fleur de peau", "avoir les nerfs en boule",
                "avoir les nerfs en pelote", "avoir un coup de sang", "avoir quelque chose sur le coeur", "blessé",
                "bouillonnant", "choqué", "coléreux", "contracté", "contrarié", "courir sur le haricot", "courroucé",
                "crispé", "d'humeur acariâtre", "d'humeur bougonne", "d'humeur bourrue", "d'humeur hostile",
                "d'humeur massacrante", "d'humeur noire", "d'humeur querelleuse", "d'humeur réprobatrice",
                "d'humeur revêche", "d'humeur ronchonneuse", "dans des transes", "dans tous ses états",
                "décharger sa bile", "dédaigneux", "défiant", "dérangé", "devenir chèvre", "échauffé", "emporté",
                "en avoir assez", "en avoir marre", "en avoir par dessus la tête", "en avoir plein le dos",
                "en avoir ras le bol", "en avoir sa claque", "en colère", "en pétard", "en rogne", "écoeuré", "énervé",
                "entêté", "envieux", "éperdu", "éperonné", "être à bout de nerf exalté", "exaspéré", "excédé", "excité",
                "fâché", "faire endêver", "faire tourner les sangs froissé", "frustré", "garder rancune grincheux",
                "grognon", "haineux", "hargneux", "hérissé", "heurté", "horripilé", "hors de ses gonds", "impatient",
                "indigné", "irascible", "irrité", "jaloux", "mal disposé", "marri mécontent", "méfiant", "morose",
                "nerveux", "offusqué", "ombrageux", "outré", "piqué au vif", "piteux", "plein d'acharnement",
                "plein d'aigreur", "plein d'animosité", "plein d'aversion", "plein d'exécration", "plein d'ire",
                "plein de bile", "plein de fiel", "plein de fièvre", "plein de frénésie", "plein de hâte",
                "plein de rancœur", "plein de rancune", "plein de récrimination", "plein de réprobation",
                "plein de ressentiment", "plein de véhémence", "porter sur les nerfs", "prendre feu",
                "prendre la mouche", "prendre ombrage", "rebêqué", "rebuté", "refroidi", "rembruni", "remonté",
                "renfermé", "rétif", "révolté", "s'échauffer la bile", "scandalisé", "se foutre en bombe",
                "se mettre en boule", "sombre", "soupçonneux", "sur les nerfs", "surexcité", "survolté", "susceptible",
                "suspicieux", "taper sur les nerfs", "tendu", "titillé", "tourmenté", "tracassé", "transi", "ulcéré",
                "vexé",

            ]
        ,

        'Fureur':

            [

                "fureur", "enragé", "fou", "furieux", "furax", "furibond", "furieux", "hors de soi", "ivre de rage",
                "poussé à bout",

            ]
        ,

        'Peur':
            [

                "peur", "affolé", "alarmé", "anesthésié", "angoissé", "anxieux", "apeuré", "aux abois",
                "aux cent coups", "avide", "avoir des sueurs froides", "avoir la frousse", "avoir la hantisse",
                "avoir la pétoche", "avoir la trouille", "avoir les foies", "avoir les grelots",
                "avoir les jetons blindé", "bloqué", "chaviré", "circonspect", "coupable", "craintif",
                "dans des affres", "dans des transes", "déchiré", "effarouché", "effrayé", "embarrassé", "en alerte",
                "en désarroi", "en détresse", "envieux", "faire tourner les sangs", "fébrile", "gêné", "hérissé",
                "impatient", "incrédule", "inquiet", "intimidé", "jaloux", "mal à l'aise", "mal assuré", "méfiant",
                "pantelant", "pessimiste", "plein d'appréhension", "plein de convoitise", "plein de hâte",
                "plein de pudeur", "plein de trac", "plus mort que vif", "préoccupé", "prudent", "réservé", "réticent",
                "sceptique", "se faire de la bile", "se faire du mauvais sang", "se faire du mouron",
                "se faire un sang d'encre", "se retourner les sangs", "se ronger les moelles", "se ronger les poings",
                "se ronger les sangs soucieux", "soupçonneux", "sur des braises", "sur des charbons ardents",
                "sur la défensive", "sur la réserve", "sur le qui-vive", "sur ses gardes", "suspicieux", "timoré",
                "tomber de haut", "transi", "traumatisé", "tremblant", "trémulant", "vulnérable",

            ]
        ,

        'Terreur':
            [

                "terreur", "épouvanté", "glacé de peur", "horrifié", "paniqué", "plein d'effroi", "plein de frousse",
                "terrifié", "terrorisé", "traumatisé",

            ]
        ,

        'Coupure avec ses émotions':

            [

                "à plat", "abattu", "abruti", "absent", "accablé", "affaibli", "alangui", "amoindri", "anesthésié",
                "apathique", "appesanti", "assourdi", "atone", "au bout du rouleau", "baisser les bras", "ballotté",
                "blasé", "bloqué", "chaviré", "claqué", "confus", "coupé", "crevé", "dans tous ses états", "débordé",
                "déchiré", "déconnecté", "démobilisé", "démonté", "démuni", "dépassé", "dépourvu", "déprimé", "déréglé",
                "désarmé", "désespéré", "désoeuvré", "détraqué", "diminué", "distant", "distrait", "divisé", "ébranlé",
                "écartelé", "effondré", "embarrassé", "embrouillé", "emmêlé", "en retrait", "en suspens", "endormi",
                "engourdi", "ennuyé", "éprouvé", "épuisé", "éreinté", "essouflé", "éteint", "étiolé", "exténué",
                "fatigué", "flageolant", "flapi", "fourbu", "harassé", "hésitant", "impuissant", "inattentif",
                "incertain", "incommodé", "inconfortable", "indécis", "indéterminé", "indifférent", "indolent",
                "inerte", "insensibilisé", "instable", "intimidé", "irrésolu", "K.-O.", "languissant", "las", "lassé",
                "léthargique", "lourd", "mal à l'aise", "mal assuré", "moulu", "n'en pouvoir plus",
                "ne pas savoir sur quel pied danser", "pantelant", "partagé", "paumé", "pensif", "perdre la boussole",
                "perdre pied", "perdu", "perplexe", "perturbé", "piteux", "plein d'atermoiement", "plein de doute",
                "plein de scrupules", "plein de tergiversation", "plein de torpeur", "plus mort que vif", "ramolli",
                "recru", "rendu", "résigné", "rêveur", "rompu", "roué de fatigue", "sans élan", "sans entrain",
                "saturé", "seul", "somnolent", "souffrant", "soûlé", "sous pression", "submergé", "sur la retenue",
                "sur les dents", "surmené", "tanné", "terrassé", "tiède", "timide", "tiraillé", "usé", "vanné",
                "vaseux", "vidé", "vulnérable",

            ]
        ,

    }

}

stop_words = {
    'en':
        [
            'a', 'able', 'about', 'across', 'after', 'all', 'almost', 'also', 'am', 'among', 'an', 'and', 'any', 'are',
            'as', 'at', 'be', 'because', 'been', 'but', 'by', 'can', 'cannot', 'could', 'dear', 'did', 'do', 'does',
            'either', 'else', 'ever', 'every', 'for', 'from', 'get', 'got', 'had', 'has', 'have', 'he', 'her', 'hers',
            'him', 'his', 'how', 'however', 'i', 'if', 'in', 'into', 'is', 'it', 'its', 'just', 'least', 'let', 'like',
            'likely', 'may', 'me', 'might', 'most', 'must', 'my', 'neither', 'no', 'nor', 'not', 'of', 'off', 'often',
            'on', 'only', 'or', 'other', 'our', 'own', 'rather', 'said', 'say', 'says', 'she', 'should', 'since', 'so',
            'some', 'than', 'that', 'the', 'their', 'them', 'then', 'there', 'these', 'they', 'this', 'tis', 'to',
            'too', 'twas', 'us', 'wants', 'was', 'we', 'were', 'what', 'when', 'where', 'which', 'while', 'who', 'whom',
            'why', 'will', 'with', 'would', 'yet', 'you', 'your',

            "\'d", "\'ll", "\'m", "\'re", "\'s", "\'t", "n\'t",
            "\'ve", "a", "aboard", "about", "above", "across", "after", "again", "against", "all", "almost",
            "alone", "along", "alongside", "already", "also", "although", "always", "am", "amid", "amidst",
            "among", "amongst", "an", "and", "another", "anti", "any", "anybody", "anyone", "anything",
            "anywhere", "are", "area", "areas", "aren't", "around", "as", "ask", "asked", "asking", "asks",
            "astride", "at", "aught", "away", "back", "backed", "backing", "backs", "bar", "barring", "be",
            "became", "because", "become", "becomes", "been", "before", "began", "behind", "being", "beings",
            "below", "beneath", "beside", "besides", "best", "better", "between", "beyond", "big", "both",
            "but", "by", "came", "can", "can't", "cannot", "case", "cases", "certain", "certainly", "circa",
            "clear", "clearly", "come", "concerning", "considering", "could", "couldn't", "daren't", "despite",
            "did", "didn't", "differ", "different", "differently", "do", "does", "doesn't", "doing", "don't",
            "done", "down", "down", "downed", "downing", "downs", "during", "each", "early", "either", "end",
            "ended", "ending", "ends", "enough", "even", "evenly", "ever", "every", "everybody", "everyone",
            "everything", "everywhere", "except", "excepting", "excluding", "face", "faces", "fact", "facts",
            "far", "felt", "few", "fewer", "find", "finds", "first", "five", "following", "for", "four",
            "from", "full", "fully", "further", "furthered", "furthering", "furthers", "gave", "general",
            "generally", "get", "gets", "give", "given", "gives", "go", "goes", "going", "good", "goods",
            "got", "great", "greater", "greatest", "group", "grouped", "grouping", "groups", "had", "hadn't",
            "has", "hasn't", "have", "haven't", "having", "he", "he'd", "he'll", "he's", "her", "here",
            "here's", "hers", "herself", "high", "high", "high", "higher", "highest", "him", "himself",
            "his", "hisself", "how", "how's", "however", "i", "i'd", "i'll", "i'm", "i've", "idem", "if",
            "ilk", "important", "in", "including", "inside", "interest", "interested", "interesting",
            "interests", "into", "is", "isn't", "it", "it's", "its", "itself", "just", "keep", "keeps",
            "kind", "knew", "know", "known", "knows", "large", "largely", "last", "later", "latest", "least",
            "less", "let", "let's", "lets", "like", "likely", "long", "longer", "longest", "made", "make",
            "making", "man", "many", "may", "me", "member", "members", "men", "might", "mightn't", "mine",
            "minus", "more", "most", "mostly", "mr", "mrs", "much", "must", "mustn't", "my", "myself",
            "naught", "near", "necessary", "need", "needed", "needing", "needn't", "needs", "neither",
            "never", "new", "new", "newer", "newest", "next", "no", "nobody", "non", "none", "noone", "nor",
            "not", "nothing", "notwithstanding", "now", "nowhere", "number", "numbers", "of", "off", "often",
            "old", "older", "oldest", "on", "once", "one", "oneself", "only", "onto", "open", "opened",
            "opening", "opens", "opposite", "or", "order", "ordered", "ordering", "orders", "other", "others",
            "otherwise", "ought", "oughtn't", "our", "ours", "ourself", "ourselves", "out", "outside", "over",
            "own", "part", "parted", "parting", "parts", "past", "pending", "per", "perhaps", "place",
            "places", "plus", "point", "pointed", "pointing", "points", "possible", "present", "presented",
            "presenting", "presents", "problem", "problems", "put", "puts", "quite", "rather", "really",
            "regarding", "right", "right", "room", "rooms", "round", "said", "same", "save", "saw", "say",
            "says", "second", "seconds", "see", "seem", "seemed", "seeming", "seems", "seen", "sees", "self",
            "several", "shall", "shan't", "she", "she'd", "she'll", "she's", "should", "shouldn't", "show",
            "showed", "showing", "shows", "side", "sides", "since", "small", "smaller", "smallest", "so",
            "some", "somebody", "someone", "something", "somewhat", "somewhere", "state", "states", "still",
            "still", "such", "suchlike", "sundry", "sure", "take", "taken", "than", "that", "that's", "the",
            "thee", "their", "theirs", "them", "themselves", "then", "there", "there's", "therefore", "these",
            "they", "they'd", "they'll", "they're", "they've", "thine", "thing", "things", "think", "thinks",
            "this", "those", "thou", "though", "thought", "thoughts", "three", "through", "throughout",
            "thus", "thyself", "till", "to", "today", "together", "too", "took", "tother", "toward",
            "towards", "turn", "turned", "turning", "turns", "twain", "two", "under", "underneath", "unless",
            "unlike", "until", "up", "upon", "us", "use", "used", "uses", "various", "versus", "very",
            "via", "vis-a-vis", "want", "wanted", "wanting", "wants", "was", "wasn't", "way", "ways", "we",
            "we'd", "we'll", "we're", "we've", "well", "wells", "went", "were", "weren't", "what", "what's",
            "whatall", "whatever", "whatsoever", "when", "when's", "where", "where's", "whereas", "wherewith",
            "wherewithal", "whether", "which", "whichever", "whichsoever", "while", "who", "who's", "whoever",
            "whole", "whom", "whomever", "whomso", "whomsoever", "whose", "whosoever", "why", "why's", "will",
            "with", "within", "without", "won't", "work", "worked", "working", "works", "worth", "would",
            "wouldn't", "ye", "year", "years", "yet", "yon", "yonder", "you", "you'd", "you'll", "you're",
            "you've", "you-all", "young", "younger", "youngest", "your", "yours", "yourself", "yourselves"
        ],

    'fr':
        [
            # 'au', 'aux', 'avec', 'ce', 'ces', 'dans', 'de', 'des', 'du', 'elle', 'en', 'et', 'eux', 'il', 'je', 'la',
            # 'le',
            # 'leur', 'lui', 'ma', 'mais', 'me', 'même', 'mes', 'moi', 'mon', 'ne', 'nos', 'notre', 'nous', 'on', 'ou',
            # 'par', 'pas', 'pour', 'qu', 'que', 'qui', 'sa', 'se', 'ses', 'son', 'sur', 'ta', 'te', 'tes', 'toi', 'ton',
            # 'tu', 'un', 'une', 'vos', 'votre', 'vous', 'c', 'd', 'j', 'l', 'à', 'm', 'n', 's', 't', 'y', 'été', 'étée',
            # 'étées', 'étés', 'étant', 'suis', 'es', 'est', 'sommes', 'être', 'êtes', 'sont', 'serai', 'seras', 'sera',
            # 'serons',
            # 'serez', 'seront', 'serais', 'serait', 'serions', 'seriez', 'seraient', 'étais', 'était', 'étions', 'étiez',
            # 'étaient', 'fus', 'fut', 'fûmes', 'fûtes', 'furent', 'sois', 'soit', 'soyons', 'soyez', 'soient', 'fusse',
            # 'fusses', 'fût', 'fussions', 'fussiez', 'fussent', 'ayant', 'eu', 'eue', 'eues', 'eus', 'ai', 'as', 'avoir',
            # 'avons', 'faut', 'veut',
            # 'avez', 'ont', 'aurai', 'auras', 'aura', 'aurons', 'aurez', 'auront', 'aurais', 'aurait', 'aurions',
            # 'auriez',
            # 'auraient', 'avais', 'avait', 'avions', 'aviez', 'avaient', 'eut', 'eûmes', 'eûtes', 'eurent', 'aie',
            # 'aies',
            # 'ait', 'ayons', 'ayez', 'aient', 'eusse', 'eusses', 'eût', 'eussions', 'eussiez', 'eussent', 'ceci', 'cela',
            # 'celà', 'cet', 'cette', 'ici', 'ils', 'les', 'leurs', 'quel', 'quels', 'quelle', 'quelles', 'sans', 'soi'
            # , 'a', 'si', 'fait', 'quelques', 'bonjour', 'fois', 'depuis', 'plus', 'ça', 'après', 'peut', 'contre',
            # 'ceux'
            #
            # , 'coutumiermessages', 'celui', 'faire', 'tout', 'ans', 'toujours', 'ca', 'très', 'tant', 'aussi', 'comme'
            # , 'oui', 'com', 'comme', 'alors', 'peux', 'doivent', 'pense', 'merci', 'car', 'fais', 'fait', 'voir', 'site'
            # , 'dit', 'dite', 'dire', 'forum', 'tiens', 'dont', 'la', 'les', 'le', 'vers', 'voit', 'dès', 'tu', 'il'
            # , 'entrepreneur', 'donc', 'écrit', 'tous', 'quoi', 'mois', 'hui', 'dois', 'avant', 'www', 'afin', 'bcp'
            # , 'font'
            # , 'etc', 'chaque', 'quelqu', 'enfin', 'quand', 'où', 'lors', 'souhait', 'moins', 'cais', 'va', 'dejà'
            # , 'vais'
            # , 'bien', 'autre', 'viens', 'toute', 'mettre', 'jamais', 'voila', 'comment', 'dis', 'deux', 'mail'
            # , 'bon', 'sais', 'veux', 'doit', 'juste', 'temps', 'lire', 'trouve', 'aime', 'venir', 'non', 'cas', 'peu'
            # , 'psy', 'vite', 'là', 'vois', 'sais', 'cas', 'fin', 'seul', 'entre', 'savoir', 'herbemessages', 'vraiment'
            # , 'encore', 'etre', 'puis', 'effet', 'sites', 'mieux', 'voilà', 'ds', 'ex', 'ouvrir', 'service', 'vis'
            # , 'cours', 'class',
            #
            # "Ap.", "Apr.", "GHz", "MHz", "USD", "a", "afin", "ah", "ai", "aie", "aient", "aies", "ait",
            # "alors", "après", "as", "attendu", "au", "au-delà", "au-devant", "aucun", "aucune", "aucuns",
            # "audit", "auprès", "auquel", "aura", "aurai", "auraient", "aurais", "aurait", "auras", "aurez",
            # "auriez", "aurions", "aurons", "auront", "aussi", "autour", "autre", "autres", "autrui", "aux",
            # "auxdites", "auxdits", "auxquelles", "auxquels", "avaient", "avais", "avait", "avant", "avec",
            # "avez", "aviez", "avions", "avoir", "avons", "ayant", "ayez", "ayons", "b", "bah", "banco",
            # "ben", "bien", "bon", "bé", "c", "c'", "c'est", "c'était", "car", "ce", "ceci", "cela",
            # "celle", "celle-ci", "celle-là", "celles", "celles-ci", "celles-là", "celui", "celui-ci",
            # "celui-là", "celà", "cent", "cents", "cependant", "certain", "certaine", "certaines", "certains",
            # "ces", "cet", "cette", "ceux", "ceux-ci", "ceux-là", "cf.", "cg", "cgr", "chacun", "chacune",
            # "chaque", "chez", "ci", "cinq", "cinquante", "cinquante-cinq", "cinquante-deux", "cinquante-et-un",
            # "cinquante-huit", "cinquante-neuf", "cinquante-quatre", "cinquante-sept", "cinquante-six",
            # "cinquante-trois", "cl", "cm", "cm²", "comme", "comment", "contre", "d", "d'", "d'après", "d'un",
            # "d'une", "dans", "de", "dedans", "dehors", "depuis", "derrière", "des", "desdites", "desdits",
            # "desquelles", "desquels", "deux", "devant", "devers", "devrait", "dg", "différentes", "différents",
            # "divers", "diverses", "dix", "dix-huit", "dix-neuf", "dix-sept", "dl", "dm", "doit", "donc",
            # "dont", "dos", "douze", "droite", "du", "dudit", "duquel", "durant", "dès", "début", "déjà",
            # "e", "eh", "elle", "elles", "en", "en-dehors", "encore", "enfin", "entre", "envers", "es",
            # "essai", "est", "et", "eu", "eue", "eues", "euh", "eurent", "eus", "eusse", "eussent", "eusses",
            # "eussiez", "eussions", "eut", "eux", "eûmes", "eût", "eûtes", "f", "fait", "faites", "fi",
            # "flac", "fois", "font", "force", "fors", "furent", "fus", "fusse", "fussent", "fusses",
            # "fussiez", "fussions", "fut", "fûmes", "fût", "fûtes", "g", "gr", "h", "ha", "han", "haut",
            # "hein", "hem", "heu", "hg", "hl", "hm", "hm³", "holà", "hop", "hormis", "hors", "huit", "hum",
            # "hé", "i", "ici", "il", "ils", "j", "j'", "j'ai", "j'avais", "j'étais", "jamais", "je",
            # "jusqu'", "jusqu'au", "jusqu'aux", "jusqu'à", "jusque", "juste", "k", "kg", "km", "km²", "l",
            # "l'", "l'autre", "l'on", "l'un", "l'une", "la", "laquelle", "le", "lequel", "les", "lesquelles",
            # "lesquels", "leur", "leurs", "lez", "lors", "lorsqu'", "lorsque", "lui", "là", "lès", "m", "m'",
            # "ma", "maint", "mainte", "maintenant", "maintes", "maints", "mais", "malgré", "me", "mes", "mg",
            # "mgr", "mil", "mille", "milliards", "millions", "mine", "ml", "mm", "mm²", "moi", "moins",
            # "mon", "mot", "moyennant", "mt", "m²", "m³", "même", "mêmes", "n", "n'", "n'avait", "n'y",
            # "ne", "neuf", "ni", "nommés", "non", "nonante", "nonobstant", "nos", "notre", "nous", "nouveaux",
            # "nul", "nulle", "nº", "néanmoins", "o", "octante", "oh", "on", "ont", "onze", "or", "ou",
            # "outre", "où", "p", "par", "par-delà", "parbleu", "parce", "parmi", "parole", "pas", "passé",
            # "pendant", "personne", "personnes", "peu", "peut", "pièce", "plupart", "plus", "plus_d'un",
            # "plus_d'une", "plusieurs", "pour", "pourquoi", "pourtant", "pourvu", "près", "puisqu'", "puisque",
            # "q", "qu", "qu'", "qu'elle", "qu'elles", "qu'il", "qu'ils", "qu'on", "quand", "quant",
            # "quarante", "quarante-cinq", "quarante-deux", "quarante-et-un", "quarante-huit", "quarante-neuf",
            # "quarante-quatre", "quarante-sept", "quarante-six", "quarante-trois", "quatorze", "quatre",
            # "quatre-vingt", "quatre-vingt-cinq", "quatre-vingt-deux", "quatre-vingt-dix", "quatre-vingt-dix-huit",
            # "quatre-vingt-dix-neuf", "quatre-vingt-dix-sept", "quatre-vingt-douze", "quatre-vingt-huit",
            # "quatre-vingt-neuf", "quatre-vingt-onze", "quatre-vingt-quatorze", "quatre-vingt-quatre",
            # "quatre-vingt-quinze", "quatre-vingt-seize", "quatre-vingt-sept", "quatre-vingt-six",
            # "quatre-vingt-treize", "quatre-vingt-trois", "quatre-vingt-un", "quatre-vingt-une", "quatre-vingts",
            # "que", "quel", "quelle", "quelles", "quelqu'", "quelqu'un", "quelqu'une", "quelque", "quelques",
            # "quelques-unes", "quelques-uns", "quels", "qui", "quiconque", "quinze", "quoi", "quoiqu'",
            # "quoique", "r", "revoici", "revoilà", "rien", "s", "s'", "sa", "sans", "sauf", "se", "seize",
            # "selon", "sept", "septante", "sera", "serai", "seraient", "serais", "serait", "seras", "serez",
            # "seriez", "serions", "serons", "seront", "ses", "seulement", "si", "sien", "sinon", "six", "soi",
            # "soient", "sois", "soit", "soixante", "soixante-cinq", "soixante-deux", "soixante-dix",
            # "soixante-dix-huit", "soixante-dix-neuf", "soixante-dix-sept", "soixante-douze", "soixante-et-onze",
            # "soixante-et-un", "soixante-et-une", "soixante-huit", "soixante-neuf", "soixante-quatorze",
            # "soixante-quatre", "soixante-quinze", "soixante-seize", "soixante-sept", "soixante-six",
            # "soixante-treize", "soixante-trois", "sommes", "son", "sont", "sous", "soyez", "soyons", "suis",
            # "suite", "sujet", "sur", "sus", "t", "t'", "ta", "tacatac", "tandis", "te", "tel", "telle",
            # "tellement", "telles", "tels", "tes", "toi", "ton", "toujours", "tous", "tout", "toute",
            # "toutefois", "toutes", "treize", "trente", "trente-cinq", "trente-deux", "trente-et-un",
            # "trente-huit", "trente-neuf", "trente-quatre", "trente-sept", "trente-six", "trente-trois", "trois",
            # "trop", "très", "tu", "u", "un", "une", "unes", "uns", "v", "valeur", "vers", "via", "vingt",
            # "vingt-cinq", "vingt-deux", "vingt-huit", "vingt-neuf", "vingt-quatre", "vingt-sept", "vingt-six",
            # "vingt-trois", "vis-à-vis", "voici", "voie", "voient", "voilà", "vont", "vos", "votre", "vous",
            # "vu", "w", "x", "y", "y'", "z", "zéro", "à", "ç'", "ça", "ès", "étaient", "étais", "était",
            # "étant", "état", "étiez", "étions", "été", "étée", "étées", "étés", "êtes", "être", "ô",


            "nouveaux", "pass\u00e9", "cinquante-quatre", "gr", "f\u00fbt", "dix-neuf", "veut", "\u00e9t\u00e9",
            "aurions", "unes", "veux", "jusqu'au", "quatre-vingt-huit", "tu", "lors", "quarante-deux", "cinquante-huit",
            "vingt-trois", "moyennant", "mainte", "aux", "te", "m\u00b2", "\u00e9t\u00e9es", "vers", "envers", "neuf",
            "voie", "sont", "tandis", "voit", "vingt-deux", "voir", "vois", "auras", "quatorze", "sans",
            "cinquante-deux", "p", "dix", "duquel", "ml", "dit", "t'", "diff\u00e9rentes", "dis", "soixante-treize",
            "force", "auquel", "chez", "faites", "hors", "toutefois", "d'une", "quelques", "tes", "font",
            "\u00e9taient", "trente-trois", "quelqu", "dix-huit", "sus", "sur", "cela", "cf.", "quels", "les", "contre",
            "mes", "lez", "o\u00f9", "forum", "k", "cette", "desdits", "quarante-et-un", "bon", "nulle",
            "quatre-vingt-sept", "via", "dire", "parmi", "e\u00fbt", "cinquante-neuf", "vis", "outre",
            "soixante-dix-sept", "cinquante-cinq", "maints", "huit", "trente-et-un", "m'", "cgr", "leurs", "fussions",
            "\u00e9tions", "seraient", "vis-\u00e0-vis", "quelques-uns", "l\u00e8s", "vous", "mois", "quarante-quatre",
            "fusses", "derri\u00e8re", "auront", "aurons", "e\u00fbtes", "hein", "ap.", "voil\u00e0", "flac", "suite",
            "cm\u00b2", "me", "mg", "ma", "devant", "f", "mm", "car", "des", "pour", "mt", "sous", "v", "ta", "avaient",
            "ceux-l\u00e0", "il", "votre", "eussent", "auraient", "quarante-trois", "parce", "cours", "six",
            "quelqu'un", "soixante-dix-neuf", "hop", "quoi", "s'", "parbleu", "quatre-vingt-six", "notre",
            "au-del\u00e0", "l'une", "\u00eatre", "viens", "j'ai", "sinon", "fi", "laquelle", "a", "trois",
            "quatre-vingt-quinze", "avant", "ghz", "quatre-vingt-dix", "q", "si", "nous", "sa", "se", "peux",
            "cinquante-sept", "auxdits", "aient", "peut", "comme", "d'apr\u00e8s", "quelles", "afin", "vite",
            "pourtant", "soixante-huit", "quatre-vingt-cinq", "le", "euh", "mais", "la", "eue", "pourvu", "ds",
            "aucuns", "eux", "quelqu'une", "mail", "fin", "lire", "mettre", "non", "\u00e9tait", "trouve", "personne",
            "\u00e9tais", "cet", "soient", "dej\u00e0", "desquels", "nos", "l'", "faire", "aucun", "avons", "l", "cinq",
            "qu'elle", "seriez", "oui", "en", "eh", "autre", "ex", "qu'", "eu", "et", "aurais", "aurait", "es",
            "autrui", "quelque", "serions", "quatre-vingt", "toute", "valeur", "\u00e9crit", "trente-six",
            "soixante-quatre", "dehors", "temps", "quarante-neuf", "que", "pendant", "d'un", "qui", "km\u00b2",
            "n\u00e9anmoins", "cinquante-trois", "selon", "quelqu'", "cais", "g", "celle-ci", "cinquante-et-un", "w",
            "n'avait", "quatre-vingt-dix-huit", "c'est", "dont", "jusqu'", "donc", "soixante-et-une", "ses", "ont",
            "millions", "droite", "service", "avec", "vingt-cinq", "\u00e0", "soixante-cinq", "avez", "toi", "ton",
            "quatre-vingt-onze", "plupart", "\u00f4", "quelques-unes", "hol\u00e0", "soixante-sept", "b", "ben",
            "fussent", "dix-sept", "dans", "merci", "r", "trente", "sera", "aime", "encore", "ait", "hm\u00b3",
            "cinquante", "devers", "telle", "aussi", "mine", "eusses", "faut", "ans", "onze", "pi\u00e8ce", "pr\u00e8s",
            "mil", "alors", "quatre-vingt-deux", "fussiez", "deux", "hum", "seront", "mille", "divers", "serons",
            "diverses", "apr.", "hui", "doit", "\u00e8s", "maintenant", "dois", "d\u00e8s", "class", "eurent", "treize",
            "doivent", "fusse", "kg", "seize", "m", "km", "\u00e9tiez", "dos", "herbemessages", "quatre-vingt-quatre",
            "puisque", "d'", "maint", "toujours", "h\u00e9", "dudit", "desquelles", "serait", "soyez", "serais",
            "quatre-vingt-neuf", "quel", "auxdites", "sien", "cependant", "ayez", "dl", "dm", "quarante-cinq", "dg",
            "qu'elles", "de", "maintes", "mgr", "mieux", "du", "m\u00eames", "l'autre", "qu", "soixante-deux", "furent",
            "h", "vingt", "leur", "sauf", "f\u00fbtes", "x", "bah", "fait", "jusque", "celles-ci", "quatre-vingt-douze",
            "fais", "nul", "quarante", "soixante-six", "j'", "vont", "sites", "bien", "chaque", "sais", "lui",
            "trente-neuf", "m\u00eame", "soixante-douze", "\u00e7a", "soixante-neuf", "j'\u00e9tais", "effet", "soyons",
            "jusqu'aux", "certaines", "je", "aupr\u00e8s", "c", "uns", "han", "puisqu'", "aucune", "etc", "s", "tous",
            "tout", "une", "vraiment", "quatre-vingt-seize", "com", "quatre-vingt-dix-sept", "comment", "ci",
            "quarante-huit", "cm", "cl", "etre", "vingt-quatre", "ca", "\u00e9tat", "cg", "ce", "coutumiermessages",
            "auriez", "vais", "seulement", "moins", "par-del\u00e0", "lorsque", "qu'il", "cent", "tr\u00e8s",
            "celui-l\u00e0", "attendu", "tiens", "certain", "quatre-vingt-trois", "venir", "celle-l\u00e0", "revoici",
            "aies", "\u00e9t\u00e9s", "e\u00fbmes", "c'", "ceux", "eut", "nonobstant", "bonjour", "quatre",
            "\u00e9t\u00e9e", "soixante", "fut", "fus", "lorsqu'", "tacatac", "auxquels", "as", "seras", "j'avais",
            "serai", "voici", "aie", "quatre-vingt-quatorze", "tant", "n'y", "aurai", "va", "cents",
            "quatre-vingt-treize", "site", "soit", "vu", "sois", "vos", "dedans", "soixante-et-un", "trente-quatre",
            "par", "pas", "quand", "puis", "revoil\u00e0", "en-dehors", "cel\u00e0", "aviez", "ils", "tellement",
            "voient", "elle", "cas", "vingt-six", "pourquoi", "soixante-et-onze", "soixante-dix", "audit", "entre", "i",
            "soixante-quinze", "lequel", "psy", "quarante-six", "y", "ces", "soixante-dix-huit", "quatre-vingt-une",
            "pense", "seul", "autour", "eus", "celles-l\u00e0", "aura", "celui-ci", "quelle", "d\u00e9but", "n",
            "voila", "enfin", "telles", "devrait", "certaine", "haut", "mot", "dite", "certains", "moi", "vingt-neuf",
            "mon", "nomm\u00e9s", "ayant", "serez", "ni", "aurez", "eussiez", "d", "c'\u00e9tait", "m\u00b3",
            "auxquelles", "cinquante-six", "mhz", "t", "l'on", "soixante-seize", "ceux-ci", "www", "autres", "trop",
            "\u00e9tant", "sommes", "au-devant", "quoique", "rien", "est", "essai", "peu", "depuis", "quinze", "avoir",
            "quiconque", "mm\u00b2", "b\u00e9", "tels", "fois", "ayons", "usd", "savoir", "vingt-huit", "quoiqu'",
            "celles", "on", "quant", "juste", "oh", "douze", "o", "plus", "souhait", "ceci", "ou", "sept", "or", "tel",
            "quarante-sept", "ouvrir", "soi", "son", "entrepreneur", "\u00e7'", "quatre-vingt-un", "vingt-sept",
            "banco", "apr\u00e8s", "heu", "chacune", "plus_d'un", "avait", "toutes", "celui", "septante", "avais",
            "suis", "eussions", "eues", "plusieurs", "hem", "y'", "desdites", "malgr\u00e9", "avions", "trente-cinq",
            "hl", "hm", "fors", "ha", "diff\u00e9rents", "hg", "\u00eates", "soixante-trois", "j", "un", "hormis", "z",
            "celle", "qu'ils", "ai", "ah", "trente-deux", "soixante-quatorze", "au", "d\u00e9j\u00e0", "milliards",
            "n\u00ba", "jamais", "trente-huit", "elles", "lesquelles", "quatre-vingt-dix-neuf", "z\u00e9ro", "ne",
            "trente-sept", "personnes", "octante", "f\u00fbmes", "l'un", "chacun", "quatre-vingts", "ici", "qu'on",
            "lesquels", "durant", "e", "nonante", "bcp", "eusse", "n'", "plus_d'une", "u", "l\u00e0", "parole", "sujet",
            "jusqu'\u00e0"

        ]
}

puntuations = [
    # "#", "@",  # Remove # and @ to identify hashtags and people mentions
    "!", "$", "%", "&", "'", "(", ")", "*", "+", ",", "-", ".", ":", ";", "<", "=", ">", "?", "[", "\\"
    , "]", "^", "_", "`", "{", "|", "}", "~", "¡", "¢", "£", "¤", "¥", "¦", "§", "¨", "©", "ª", "«", "¬", "®", "¯",
    "°", "±", "²", "³", "´", "µ", "¶", "·", "¸", "¹", "º", "»", "¼", "½", "¾", "¿", "ʰ", "ʱ", "ʲ", "ʳ", "ʴ", "ʵ"
    , "ʶ", "ʷ", "ʸ", "ʹ", "ʺ", "ʻ", "ʼ", "ʽ", "ʾ", "ʿ", "ˀ", "ˁ", "˂", "˃", "˄", "˅", "ˆ", "ˇ", "ˈ", "ˉ", "ˊ",
    "ˋ", "ˌ", "ˍ", "ˎ", "ˏ", "ː", "ˑ", "˒", "˓", "˔", "˕", "˖", "˗", "˘", "˙", "˚", "˛", "˜", "˝", "˞", "˟", "ˠ",
    "ˡ", "ˢ", "ˣ", "ˤ", "˥", "˦", "˧", "˨", "˩", "˪", "˫", "ˬ", "˭", "ˮ", "˯", "˰", "˱", "˲", "˳", "˴",
    "˵", "˶", "˷", "˸", "˹", "˺", "˻", "˼", "˽", "˾", "˿",
]

tagsetsraw = {
    'noun': ['NN', 'NNP', 'NNS', 'NNPS'],
    'adjective': ['JJ', 'JJR', 'JJS'],
    'verb': ['VB', 'VBP', 'VBD', 'VBZ', 'VBN', 'VBG'],
    'adverb': ['RB', 'RBR', 'RBS']
}

tagset = {
    # noun
    'NN': {'action': None, "tag": 'noun'},
    'NNS': {'action': 'toSingle', "tag": 'noun'},
    'NNP': {'action': None, "tag": 'noun'},
    'NNPS': {'action': 'toSingle', "tag": 'noun'},

    # adjective
    'JJ': {'action': None, "tag": 'adjective'},
    'JJR': {'action': None, "tag": 'adjective'},
    'JJS': {'action': None, "tag": 'adjective'},

    # verb
    'VB': {'action': None, "tag": 'verb'},
    'VBP': {'action': 'toInfinitive', "tag": 'verb'},
    'VBD': {'action': 'toInfinitive', "tag": 'verb'},
    'VBZ': {'action': 'toInfinitive', "tag": 'verb'},
    'VBN': {'action': 'toInfinitive', "tag": 'verb'},
    'VBG': {'action': 'toInfinitive', "tag": 'verb'},

    # adverb
    'RB': {'action': None, "tag": 'adverb'},
    'RBR': {'action': None, "tag": 'adverb'},
    'RBS': {'action': None, "tag": 'adverb'},
}

COLLECTION_NAME_LIST = ["adjectives", "adverbs", "verbs", "nouns", "concepts", "keywords", "terms"]

COLLECTION_FIELD_MAP = {
    "adjectives": {
        "word": "word",
    },
    "adverbs": {
        "word": "word",
    },
    "verbs": {
        "word": "word",
    },
    "nouns": {
        "word": "word",
    },

    "concepts": {
        "word": "concept",
    },

    "keywords": {
        "word": "keyword",
    },
    "terms": {
        "word": "term",
    },
}

COLLECTION_ASSOCIATES_NAME_LIST = [

    'verbatimconcepts',
    'verbatimterms',
    'verbatimkeywords',
    #
    # 'verbatimadjectives',
    # 'verbatimadverbs',
    # 'verbatimnouns',
    # 'verbatimverbs',
    # 'verbatimtags',
]

COLLECTION_ASSOCIATES_FIELD_MAP = {

    'verbatimadjectives': {
        "words": "words",
        "dbname": "wcadjectives"
    },

    'verbatimadverbs': {
        "words": "words",
        "dbname": "wcadverbs"
    },

    'verbatimconcepts': {
        "words": "concepts",
        "dbname": "wcconcepts"
    },

    'verbatimkeywords': {
        "words": "keywords",
        "dbname": "wckeywords"
    },

    'verbatimnouns': {
        "words": "words",
        "dbname": "wcnouns"
    },

    'verbatimtags': {
        "words": "tags",
        "dbname": "wctags"
    },

    'verbatimterms': {
        "words": "terms",
        "dbname": "wcterms"
    },

    'verbatimverbs': {
        "words": "words",
        "dbname": "wcverbs"
    },

}
