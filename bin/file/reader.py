from bin.insight import normalizer
import csv
import pandas as pd


class FileReader(object):
    def __init__(self, _file):
        self._file = _file

    def read(self):
        return self._file.readlines()


class ColumnFileReader(FileReader):
    simplenormalizer = None

    def __init__(self, lang, _file):
        super(ColumnFileReader, self).__init__(_file)
        self.lang = lang
        self.simplenormalizer = normalizer.SimpleNormalizer(lang)

    def read(self, ifheader=True, seperator=',', quotechar='"'):
        _iter = 0
        # header = [self.simplenormalizer.normalize_word(text) for text in raw_content[0].split(seperator)]
        content = []
        header = None
        with self._file as csvfile:
            spamreader = csv.reader(csvfile, delimiter=seperator, quotechar=quotechar)
            for row in spamreader:
                if _iter == 0:
                    # header
                    header = [self.simplenormalizer.normalize_word(text) for text in row]
                else:
                    # body content
                    item = {}
                    for h, c in zip(header, row):
                        item[h] = c
                    content.append(item)
                _iter += 1
        return content


class PandaCSVReader(FileReader):
    def read(self, columns=None):
        if columns is None:
            columns = ['indexnb', 'verbatim']
        content = []
        df = pd.read_csv(self._file.name)

        for i, row in df.iterrows():
            d = {}
            for v, c in zip(row, columns):
                d[c] = v
            content.append(d)

        return content
