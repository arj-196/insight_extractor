class FileWriter(object):
    NL = '\n'

    def encode(self, string):
        try:
            return string.encode('utf8')
        except:
            return None


class ColumnWriter(FileWriter):
    def __init__(self, _file):
        self.file = _file

    def __del__(self):
        self.file.close()

    def write(self, columns):
        string = self.encode(",".join(columns)) + self.NL
        if string:
            self.file.write(string)
