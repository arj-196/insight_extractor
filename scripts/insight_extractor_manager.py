# -*- coding: utf-8 -*-
import sys, traceback
import bin.insight.insightextractor as extractor
from bin.insight.location import LocationExtractor
from datetime import datetime
import os
from pymongo import MongoClient
from bin.insight import resources as rs
from bin.mailer import mailer
from bin.mailer import resources as mrs
from pattern.vector import words, count
startTime = datetime.now()

OPTIONS = {
    'dev': {
        'download_dir': '/Users/arj/www/verbatim_explorer/public/tmp/uploads/',
        'upload_dir': '/Users/arj/app/insight_extractor/tmp/uploads/',
        'upload_server': 'db',
    },
    'prod': {
        'download_dir': '/Users/arj/www/verbatim_explorer/public/tmp/uploads/',  # TODO change
        'upload_dir': '/home/insighte/processing/insight_extractor/tmp/uploads/',
        'upload_server': 'db',
    },
}


def schedule_extraction(mode):
    client = MongoClient(rs.HOST)
    # checking if new job exists
    nbpendingjobs = client.schedule.datasets.count()
    if nbpendingjobs > 0:
        # if job exists
        job = client.schedule.datasets.find_one()

        # transfer file to db server
        if mode == 'prod':
            cmd_transfer = 'scp ' + OPTIONS[mode]['download_dir'] + job['filename'] + ' ' \
                           + OPTIONS[mode]['upload_server'] + ':' + OPTIONS[mode]['upload_dir']
        else:
            cmd_transfer = 'cat ' + OPTIONS[mode]['download_dir'] + job['filename'] + ' >> ' \
                            + OPTIONS[mode]['upload_dir'] + job['filename']

        os.system(cmd_transfer)

        try:
            # insert entry in db
            client.schedule.extractions.insert(job)
            # remove job from schedule dataset
            client.schedule.datasets.remove({'_id': job['_id']})
        except:
            pass


def start_extraction(mode):
    client = MongoClient(rs.HOST)
    # checking if new job exists
    nbpendingjobs = client.schedule.extractions.count()
    if nbpendingjobs > 0:

        # if available resources
        if client.schedule.active_processes.count() < 3:
            # if job exists
            job = client.schedule.extractions.find_one()
            if job:
                # declare process active
                client.schedule.active_processes.insert(job)

                # remove from schedule
                client.schedule.extractions.remove({'_id': job['_id']})

                # extractor information
                inputfile = OPTIONS[mode]['upload_dir'] + job['filename']
                topicname = job['topic']
                project = job['project']
                company = job['company']
                user = job['user']
                lang = job['lang']

                #
                print 'starting extracting for', project, topicname, lang, company, user

                # execute the extraction
                execute_insight_extractor(inputfile, topicname, project, company, user, lang)
                # execute ner extraction
                execute_ner_extractor(client, topicname)
                # execute location
                execute_location_extractor(topicname)

                # declare process inactive
                client.schedule.active_processes.remove({'_id': job['_id']})

                # send mail to client
                user = client.client.users.find_one({'_id': job['user']})

                if user and 'email' in user:
                    Mailer = mailer.Mailer()
                    Mailer.send(
                        mrs.INSIGHTADDR,
                        user['email'],
                        'Dataset Uploaded',
                        'Dataset ' + job['topic'] + ' is uploaded!'
                    )


def execute_insight_extractor(inputfile, topicname, project, company, user, lang):
    # create manager
    manager = extractor.InsightExtractorManager(
        inputfile=inputfile, topic=topicname, project=project, company=company, user=user, lang=lang,
        extractor=extractor.BaseInsightExtactor
    )
    manager.extract()


def execute_ner_extractor(client, topic):
    global_organizations = [d['e'].encode('utf8').strip().lower() for d in
                            client.cache.entities.find({'t': 'ORGANIZATION'}, no_cursor_timeout=True)]
    global_locations = [d['e'].encode('utf8').strip().lower() for d in
                        client.cache.entities.find({'t': 'LOCATION'}, no_cursor_timeout=True)]

    lang = client.indexes.settings.find_one({"topic": topic})['lang']
    stop_words = rs.stop_words[lang]
    organizations = list(set(global_organizations) - set(stop_words))
    locations = list(set(global_locations) - set(stop_words))

    for s in client.indexes.verbatims.find({"topic": topic}, {"_id": 1, 'verbatim': 1}, no_cursor_timeout=True):
        try:
            ws = [w.encode('utf8').strip().lower() for w in words(s['verbatim'])]
            cs = count(ws)
            bad_words = list(set(ws) & set(stop_words))
            good_words = list(set(ws) - set(bad_words))

            # extract entities
            found_organizations = list(set(good_words) & set(organizations))
            found_locations = list(set(good_words) & set(locations))

            # format entities
            orgs = {}
            for org in found_organizations:
                _org = org.strip().lower()
                if _org in cs:
                    orgs[org] = cs[_org]

            locs = {}
            for org in found_locations:
                _org = org.strip().lower()
                if _org in cs:
                    locs[org] = cs[_org]

            if orgs or locs:
                entities = {
                    "LOCATION": locs,
                    "ORGANIZATION": orgs
                }

                # saving
                client.indexes.verbatimners.update(
                    {'_id': s['_id']},
                    {
                        'topic': topic,
                        'entities': entities
                    },
                    upsert=True
                )
        except:
            e = sys.exc_info()[0]
            print ("ERROR %s", e)
            traceback.print_exc(file=sys.stdout)

    # finally a little cleaning
    client.indexes.verbatimners.remove({"entities.ORGANIZATION": {}, "entities.LOCATION": {}})


def execute_location_extractor(topic):
    lextractor = LocationExtractor('en')
    try:
        lextractor.extract(topic, ifsave=True)
    except:
        e = sys.exc_info()[0]
        print ("ERROR %s", e)
        traceback.print_exc(file=sys.stdout)


if __name__ == '__main__':
    if sys.argv[1:]:
        if sys.argv[1] == 'schedule':
            schedule_extraction(sys.argv[2])
        elif sys.argv[1] == 'extract':
            start_extraction(sys.argv[2])

#
# python insight_extractor_manager.py schedule dev
# python insight_extractor_manager.py extract dev
#
